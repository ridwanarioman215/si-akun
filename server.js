const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config();
const db = require("./src/config/database");
const https = require("https");
const fs = require("fs");
//const db = require('./src/config/database');

const path = require('path');
const router = require('./src/routes/index');
port = process.env.PORT || 4200,

app.use(express.urlencoded({ extended: false}));
app.use(express.json());
app.use(cors());

// CERTIFICATE KEYS
var key = fs.readFileSync(__dirname + "/src/cert/ut2022-upd.key");
var cert = fs.readFileSync(__dirname + "/src/cert/ut2022-upd.crt");
var options = {
  key: key,
  cert: cert,
};



app.use("/", router);
app.use("/siakun/file/pdf-bank", express.static(path.join(__dirname,"/src/public/pdf/bank")) );

db.sync()
  .then(() => {
    var server = https.createServer(options, app);
    server.listen(4200, console.log(`server running...huy`));
  })
  .catch((err) => {
    console.log("error database", err);
  });


//app.listen(port);
console.log('Akun Standar MAK, Listening to port: ' + port);