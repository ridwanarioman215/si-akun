const express = require("express");
const router = express.Router();
const { validationResult } = require("express-validator");
const { jsonFormat } = require("../utils/jsonFormat");
const {
    index,
    show,
    showaplikasi,
    store,
    update,
    hapus } = require("../controllers/ref_master_jurnal.controller");
 const jurnalSchema = require("../request/ref_master_jurnal.validasi");

router.get("/show-all", index);
router.get("/show/:kode_penyeimbang", show);
router.get("/show-aplikasi/:kode_aplikasi", showaplikasi);
router.post("/store",jurnalSchema.store,(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.statusCode = 401
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    return next()
},store);
router.put("/update/:kode_penyeimbang",jurnalSchema.update,(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.statusCode = 401
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    return next()
},update);
router.delete("/destroy/:kode_penyeimbang")


module.exports = router;
