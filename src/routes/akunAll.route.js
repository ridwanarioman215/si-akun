const express = require("express");
const router = express.Router();
const {
    getAllAkunNest,
    getAllAkun
} = require("../controllers/akunAll.controller");

router.get("/nestAkun", getAllAkunNest);
router.get("/All", getAllAkun);


module.exports = router;
