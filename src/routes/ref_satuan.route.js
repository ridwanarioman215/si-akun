const express = require("express");
const router = express.Router();
const {
    getAll,create
} = require("../controllers/ref_satuan.controller");

router.get("/get-all", getAll);
router.post("/create", create);

module.exports = router;
