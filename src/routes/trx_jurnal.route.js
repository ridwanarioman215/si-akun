const express = require("express");
const router = express.Router();
const { validationResult } = require("express-validator");
const { jsonFormat } = require("../utils/jsonFormat");
const {
    nestedJurnal,nestedNewJurnal,filterJurnal,jurnalById,storePlanExpend,storeRealExpend,storeReversExpend,
    createplanpengeluaran,createreversal,createpengeluaran,rkaakandipakai,rkaterealisasi,rkatidakjadidipakai,
    rkatercatat,indexAplikasi
} = require("../controllers/trx_jurnal.controller");
const jurnalSchema = require("../request/trx_jurnal.validasi");


router.get("/nested-jurnal", nestedJurnal);
router.get("/data-filter/:tanggal_awal/:tanggal_akhir", nestedNewJurnal);
router.get("/filter/:tanggal_awal/:tanggal_akhir", filterJurnal);
router.get("/data-by-id/:kode_transaksi", jurnalById);
router.post("/create-pengeluaran-plan",jurnalSchema.createplanValidation, createplanpengeluaran);
router.post("/create-reversal-plan",jurnalSchema.createreversalValidation, createreversal);
router.post("/create-pengeluaran", createpengeluaran);
router.post("/create-pengeluaran-plan-new",jurnalSchema.createplannewValidation,(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.statusCode = 401
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    return next()
},storePlanExpend);
router.post("/create-pengeluaran-new",jurnalSchema.createnewValidation,(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.statusCode = 401
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    return next()
},storeRealExpend);

router.post("/create-reversal-new",jurnalSchema.createnewreversalValidation,(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.statusCode = 401
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    return next()
},storeReversExpend);

router.get("/rkaakandipakai/:tahun", rkaakandipakai);
router.get("/rkaterealisasi/:tahun", rkaterealisasi);
router.get("/rkatidakjadidipakai/:tahun", rkatidakjadidipakai);
router.get("/rkatercatat/:tahun", rkatercatat);
router.get("/show/:aplikasi", indexAplikasi);

module.exports = router;
