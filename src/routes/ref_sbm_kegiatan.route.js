const express = require("express");
const router = express.Router();
const {
    getAll,getById,byFKSBM,forEbudgeting,getAllSbm,byidSBM, getAllSbmnested,getAllBentuk,byidBentuk,forEbudgetingnew,
    createData,createSBM,createBentuk,
    updateData,updateSBM,updateBentuk,
    hapusData,deleteSBM,deleteBentuk
} = require("../controllers/ref_sbm_kegiatan.controller");
const refsbmSchema = require("../request/ref_sbm_kegiatan.validasi")

router.get("/get-all", getAll);
router.get("/get-selection", forEbudgeting);
router.get("/get-selection-new", forEbudgetingnew);
router.get("/get-by-id/:kode_sbm", getById);
router.post("/create-data",refsbmSchema.create,createData);
router.put("/update-data",refsbmSchema.update,updateData);
router.delete("/delete-data",refsbmSchema.hapus,hapusData);

router.get("/get-sbm", getAllSbm);
router.get("/get-sbm/:kode_sbm", byidSBM);
router.get("/get-sbm-by-fk/:kode_bentuk_kegiatan", byFKSBM);
router.post("/create-sbm",refsbmSchema.createSbmAkun,createSBM);
router.put("/update-sbm",refsbmSchema.updateSbmAkun,updateSBM);
router.delete("/delete-sbm/:kode_sbm",deleteSBM);

router.get("/get-sbm-nested", getAllSbmnested);
router.get("/get-bentuk", getAllBentuk);
router.get("/get-bentuk/:kode_bentuk_kegiatan", byidBentuk);
router.post("/create-bentuk",refsbmSchema.createBentukKegiatan,createBentuk);
router.put("/update-bentuk",refsbmSchema.updateBentukKegiatan,updateBentuk);
router.delete("/delete-bentuk/:kode_bentuk_kegiatan",deleteBentuk);
module.exports = router;
