const express = require("express");
const router = express.Router();
const {
    getAllAkunMak,
    getAkunMakById,
    // getAllAkun
} = require("../controllers/akunMak.controller");

router.get("/akunmak", getAllAkunMak);
// router.get("/all", getAllAkun);
router.get("/akunmak/:id", getAkunMakById);

module.exports = router;
