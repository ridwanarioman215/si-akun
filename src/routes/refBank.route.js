const express = require("express");
const router = express.Router();
const { jsonFormat } = require("../utils/jsonFormat");
const {
    getAll,getById,getByKodeUnit,
    createBank,
    updateData
} = require("../controllers/refBank.controller");
const refBankSchema = require("../request/refbank.validasi")
const path = require("path");
const fs = require("fs");
const multer = require("multer");

// CONFIGURATION UPLOAD
const maxSize = 0.5 * 1024 * 1024;
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, "./src/public/pdf/bank/");
    },
    filename: (req, file, cb) => {
      const nama_image ="siakun_"+Date.now() + path.parse(file.originalname).ext;
      cb(null, nama_image);
    },
  });

  const multerfilter = (res,file,next) =>{
    const arrJenisFile = ["pdf","jpeg","png","jpg"]
    if(arrJenisFile.includes(file.mimetype.split('/')[1])){
      next(null,true)
    }else{
      next(null,false)

    } 
  //   if (
  //     file.mimetype == "image/png" ||
  //     file.mimetype == "image/jpg" ||
  //     file.mimetype == "image/jpeg"
  //   ) {
  //     cb(null, true);
  //   } else {
  //     cb(null, false);
  //     return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
  //   }
  // },
  // limits: { fileSize: maxSize }

  }

  const upload = multer({
    storage:storage,
    fileFilter: (req, file, cb) => {
      if (
        file.mimetype == "image/png" ||
        file.mimetype == "image/jpg" ||
        file.mimetype == "image/jpeg" ||
        file.mimetype == "application/pdf"
      ) {
        cb(null, true);
      } else {
        cb(null, false);
        //return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
      }
    },
    limits: { fileSize: maxSize },
  })

  
  router.post("/register",upload.single('dokumen'),refBankSchema.createBank,createBank)
  router.put("/ubah-data-bank",refBankSchema.updateBank,updateData)
  router.get("/get-all-bank", getAll);
  router.get("/get-by-nomor-rekening/:nomor_rekening", getById);
  router.get("/get-by-kode-unit/:kode_unit", getByKodeUnit);


module.exports = router;
