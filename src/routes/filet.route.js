const express = require("express");
const router = express.Router();

const path = require('path');
router.get("/pdf-bank/", express.static(path.join(__dirname,"../src/public/pdf/bank/")) );


module.exports = router;
