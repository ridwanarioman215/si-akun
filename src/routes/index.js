const router = require("express").Router();

const akunMakRoute = require('./akunMak.route');
const akunAll = require('./akunAll.route');
const trxJurnal = require('./trx_jurnal.route');
const refMasterJurnal = require('./ref_master_jurnal.router');
const basCUD = require('./basCUD.route');
const refBank = require('./refBank.route');
const filer = require('./filet.route');
const sbmKegiatan = require('./ref_sbm_kegiatan.route');
const satuanKegiatan = require('./ref_satuan.route');
const RKAJurnal = require('./sharingrka.route');
const errorHandler = require('../utils/errorHandler')
const route = "/siakun/apiv1";
const internal = "/siakun/internal";
router.use(route+'/', akunMakRoute, akunAll);
router.use(internal+'/jurnal', trxJurnal);
router.use(route+'/jurnal', trxJurnal);
router.use(internal+'/bas-CUD', basCUD);
router.use(internal+'/ref-bank', refBank);
router.use(internal+'/file', filer);
router.use(internal+'/sbm-kegiatan', sbmKegiatan);
router.use(internal+'/satuan-kegiatan', satuanKegiatan);
router.use(internal+'/master-jurnal', refMasterJurnal);
router.use(route+'/sbm-kegiatan', sbmKegiatan);
router.use(route+'/bas-CUD', basCUD);
router.use(route+'/sharing', RKAJurnal);

// ERROR HANDLER
router.use(errorHandler);

module.exports = router;

