const express = require("express");
const router = express.Router();
const {
    creatSub1,creatSub2,creatSub3,creatSub4,creatSub5,creatSub6,
    updaturaianSub1,updaturaianSub2,updaturaianSub3,updaturaianSub4,updaturaianSub5,updaturaianSub6,
    updataktifSub1,updataktifSub2,updataktifSub3,updataktifSub4,updataktifSub5,updataktifSub6,
    nestedallsub,nestedAktif,
    getAllSub1,getAllSub2,getAllSub3,getAllSub4,getAllSub5,getAllSub6,getSelectionSub6,
    getbyIdSub1,getbyIdSub2,getbyIdSub3,getbyIdSub4,getbyIdSub5,getbyIdSub6,
    getbyFKSub1,getbyFKSub2,getbyFKSub3,getbyFKSub4,getbyFKSub5,getbyFKSub6,
    selectionNormalitas
} = require("../controllers/basCUD.controller");
const basCUDSchema = require("../request/basCUD.validasi");

router.post("/create-sub1",basCUDSchema.createS1, creatSub1);
router.post("/create-sub2",basCUDSchema.createS2, creatSub2);
router.post("/create-sub3",basCUDSchema.createS3, creatSub3);
router.post("/create-sub4",basCUDSchema.createS4, creatSub4);
router.post("/create-sub5",basCUDSchema.createS5, creatSub5);
router.post("/create-sub6",basCUDSchema.createS6, creatSub6);
router.put("/update-uraian-sub1",basCUDSchema.updateUraianS1, updaturaianSub1);
router.put("/update-uraian-sub2",basCUDSchema.updateUraianS2, updaturaianSub2);
router.put("/update-uraian-sub3",basCUDSchema.updateUraianS3, updaturaianSub3);
router.put("/update-uraian-sub4",basCUDSchema.updateUraianS4, updaturaianSub4);
router.put("/update-uraian-sub5",basCUDSchema.updateUraianS5, updaturaianSub5);
router.put("/update-uraian-sub6",basCUDSchema.updateUraianS6, updaturaianSub6);
router.put("/update-aktif-sub1",basCUDSchema.updateAktifS1, updataktifSub1);
router.put("/update-aktif-sub2",basCUDSchema.updateAktifS2, updataktifSub2);
router.put("/update-aktif-sub3",basCUDSchema.updateAktifS3, updataktifSub3);
router.put("/update-aktif-sub4",basCUDSchema.updateAktifS4, updataktifSub4);
router.put("/update-aktif-sub5",basCUDSchema.updateAktifS5, updataktifSub5);
router.put("/update-aktif-sub6",basCUDSchema.updateAktifS6, updataktifSub6);
router.get("/nested-all", nestedallsub);
router.get("/get-all-sub1", getAllSub1);
router.get("/get-all-sub2", getAllSub2);
router.get("/get-all-sub3", getAllSub3);
router.get("/get-all-sub4", getAllSub4);
router.get("/get-all-sub5", getAllSub5);
router.get("/get-all-sub6", getAllSub6);
router.get("/get-select-sub6", getSelectionSub6);
router.get("/get-byid-sub1/:kode_akun_1", getbyIdSub1);
router.get("/get-byid-sub2/:kode_akun_2", getbyIdSub2);
router.get("/get-byid-sub3/:kode_akun_3", getbyIdSub3);
router.get("/get-byid-sub4/:kode_akun_4", getbyIdSub4);
router.get("/get-byid-sub5/:kode_akun_5", getbyIdSub5);
router.get("/get-byid-sub6/:kode_akun_6", getbyIdSub6);
router.get("/get-select-normalitas",selectionNormalitas);
router.get("/nested-aktif",nestedAktif)
router.get("/get-byfk-sub2/:kode_akun_1", getbyFKSub2);
router.get("/get-byfk-sub3/:kode_akun_2", getbyFKSub3);
router.get("/get-byfk-sub4/:kode_akun_3", getbyFKSub4);
router.get("/get-byfk-sub5/:kode_akun_4", getbyFKSub5);
router.get("/get-byfk-sub6/:kode_akun_5", getbyFKSub6);

module.exports = router;
