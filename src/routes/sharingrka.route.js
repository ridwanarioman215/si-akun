const express = require("express");
const router = express.Router();
const { validationResult } = require("express-validator");
const { jsonFormat } = require("../utils/jsonFormat");
const {
    rkatercatat,showrkatercatat
} = require("../controllers/sharingrka.controller");

router.get("/rka-pertahun/:tahun", rkatercatat);
router.get("/rka-detail/:tahun/:kode_rkatu/:bulan_akhir", showrkatercatat);

module.exports = router;
