const { check } = require("express-validator");

exports.create = [
  check("kode_satuan").notEmpty().withMessage("kode_satuan harus di isi."),
  check("kode_akun_6").notEmpty().withMessage("kode_akun_6 harus di isi."),
  check("jumlah_sbm").notEmpty().withMessage("jumlah_sbm harus di isi.")
];

exports.update = [
    check("kode_sbm").notEmpty().withMessage("kode_sbm harus di isi."),
    check("kode_satuan").notEmpty().withMessage("kode_satuan harus di isi."),
    check("kode_akun_6").notEmpty().withMessage("kode_akun_6 harus di isi."),
    check("jumlah_sbm").notEmpty().withMessage("jumlah_sbm harus di isi.")
  ];


  exports.hapus = [
    check("kode_sbm").notEmpty().withMessage("kode_sbm harus di isi.")
  ];

  exports.createSbmAkun = [
    check("kode_bentuk_kegiatan").notEmpty().withMessage("kode_bentuk_kegiatan harus di isi."),
    check("komponen").notEmpty().withMessage("komponen harus di isi."),
    check("kode_satuan").notEmpty().withMessage("kode_satuan harus di isi."),
    check("kode_akun_6").notEmpty().withMessage("Kode_akun_6 harus di isi."),
    check("satuan_biaya").notEmpty().withMessage("satuan_biaya harus di isi."),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
    check("ucr").notEmpty().withMessage("ucr harus di isi."),
  ];

  exports.updateSbmAkun = [
    check("kode_sbm").notEmpty().withMessage("kode_sbm harus di isi."),
    check("kode_bentuk_kegiatan").notEmpty().withMessage("kode bentuk kegiatan harus di isi."),
    check("komponen").notEmpty().withMessage("komponen harus di isi."),
    check("kode_satuan").notEmpty().withMessage("kode_satuan harus di isi."),
    check("kode_akun_6").notEmpty().withMessage("Kode_akun_6 harus di isi."),
    check("satuan_biaya").notEmpty().withMessage("satuan_biaya harus di isi."),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
    check("uch").notEmpty().withMessage("uch harus di isi."),
  ];

  exports.createBentukKegiatan = [
    check("uraian").notEmpty().withMessage("uraian harus di isi."),
    check("ucr").notEmpty().withMessage("ucr harus di isi."),
  ];

  exports.updateBentukKegiatan = [
    check("kode_bentuk_kegiatan").notEmpty().withMessage("kode_bentuk_kegiatan harus di isi."),
    check("uraian").notEmpty().withMessage("uraian harus di isi."),
    check("uch").notEmpty().withMessage("uch harus di isi."),
  ];