const { check } = require("express-validator");

exports.createS1 = [
  check("uraian_akun_1").notEmpty().withMessage("uraian_akun_1 harus di isi.")
];

exports.createS2 = [
    check("kode_akun_1").notEmpty().withMessage("kode_akun_1 harus di isi."),
    check("kode_akun_1").isInt().withMessage("kode_akun_1 harus berbentuk number."),
    check("kode_akun_1").isLength({ min: 1,max:1 }).withMessage("kode_akun_1 hanya 1 angka yang perlu diinput."),
    check("uraian_akun_2").notEmpty().withMessage("uraian_akun_2 harus di isi."),
  ];

  exports.createS3 = [
    check("kode_akun_2").notEmpty().withMessage("kode_akun_2 harus di isi."),
    check("kode_akun_2").isInt().withMessage("kode_akun_2 harus berbentuk number."),
    check("kode_akun_2").isLength({ min: 2,max:2 }).withMessage("kode_akun_2 hanya 2 angka yang perlu diinput."),
    check("uraian_akun_3").notEmpty().withMessage("uraian_akun_3 harus di isi."),
  ];

  exports.createS4 = [
    check("kode_akun_3").notEmpty().withMessage("kode_akun_3 harus di isi."),
    check("kode_akun_3").isInt().withMessage("kode_akun_3 harus berbentuk number."),
    check("kode_akun_3").isLength({ min: 3,max:3 }).withMessage("kode_akun_3 hanya 3 angka yang perlu diinput."),
    check("uraian_akun_4").notEmpty().withMessage("uraian_akun_4 harus di isi."),
  ];

  exports.createS5 = [
    check("kode_akun_4").notEmpty().withMessage("kode_akun_4 harus di isi."),
    check("kode_akun_4").isInt().withMessage("kode_akun_4 harus berbentuk number."),
    check("kode_akun_4").isLength({ min: 4,max:4 }).withMessage("kode_akun_4 hanya 4 angka yang perlu diinput."),
    check("uraian_akun_5").notEmpty().withMessage("uraian_akun_5 harus di isi."),
  ];

  exports.createS6 = [
    check("kode_akun_5").notEmpty().withMessage("kode_akun_5 harus di isi."),
    check("kode_akun_5").isInt().withMessage("kode_akun_5 harus berbentuk number."),
    check("kode_akun_5").isLength({ min: 5,max:5 }).withMessage("kode_akun_5 hanya 5 angka yang perlu diinput."),
    check("uraian_akun_6").notEmpty().withMessage("uraian_akun_6 harus di isi."),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
  ];

  exports.updateUraianS1 = [
    check("kode_akun_1").notEmpty().withMessage("kode_akun_1 harus di isi."),
    check("kode_akun_1").isInt().withMessage("kode_akun_1 harus berbentuk number."),
    check("kode_akun_1").isLength({ min: 1,max:1 }).withMessage("kode_akun_1 hanya 1 angka yang perlu diinput."),
    check("uraian_akun_1").notEmpty().withMessage("uraian_akun_1 harus di isi."),
    check("normalitas").notEmpty().withMessage("normalitas harus di isi."),
  ];

  exports.updateUraianS2 = [
    check("kode_akun_2").notEmpty().withMessage("kode_akun_2 harus di isi."),
    check("kode_akun_2").isInt().withMessage("kode_akun_2 harus berbentuk number."),
    check("kode_akun_2").isLength({ min: 2,max:2 }).withMessage("kode_akun_2 hanya 2 angka yang perlu diinput."),
    check("uraian_akun_2").notEmpty().withMessage("uraian_akun_2 harus di isi."),
  ];

  exports.updateUraianS3 = [
    check("kode_akun_3").notEmpty().withMessage("kode_akun_3 harus di isi."),
    check("kode_akun_3").isInt().withMessage("kode_akun_3 harus berbentuk number."),
    check("kode_akun_3").isLength({ min: 3,max:3 }).withMessage("kode_akun_3 hanya 3 angka yang perlu diinput."),
    check("uraian_akun_3").notEmpty().withMessage("uraian_akun_3 harus di isi."),
  ];

  exports.updateUraianS4 = [
    check("kode_akun_4").notEmpty().withMessage("kode_akun_4 harus di isi."),
    check("kode_akun_4").isInt().withMessage("kode_akun_4 harus berbentuk number."),
    check("kode_akun_4").isLength({ min: 4,max:4 }).withMessage("kode_akun_4 hanya 4 angka yang perlu diinput."),
    check("uraian_akun_4").notEmpty().withMessage("uraian_akun_4 harus di isi."),
  ];

  exports.updateUraianS5 = [
    check("kode_akun_5").notEmpty().withMessage("kode_akun_5 harus di isi."),
    check("kode_akun_5").isInt().withMessage("kode_akun_5 harus berbentuk number."),
    check("kode_akun_5").isLength({ min: 5,max:5 }).withMessage("kode_akun_5 hanya 5 angka yang perlu diinput."),
    check("uraian_akun_5").notEmpty().withMessage("uraian_akun_5 harus di isi."),
  ];

  exports.updateUraianS6 = [
    check("kode_akun_6").notEmpty().withMessage("kode_akun_6 harus di isi."),
    check("kode_akun_6").isInt().withMessage("kode_akun_6 harus berbentuk number."),
    check("kode_akun_6").isLength({ min: 6,max:6 }).withMessage("kode_akun_6 hanya 6 angka yang perlu diinput."),
    check("uraian_akun_6").notEmpty().withMessage("uraian_akun_6 harus di isi."),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
  ];

  exports.updateAktifS1 = [
    check("kode_akun_1").notEmpty().withMessage("kode_akun_1 harus di isi."),
    check("kode_akun_1").isInt().withMessage("kode_akun_1 harus berbentuk number."),
    check("kode_akun_1").isLength({ min: 1,max:1 }).withMessage("kode_akun_1 hanya 1 angka yang perlu diinput."),
    check("aktif").notEmpty().withMessage("aktif harus di isi."),
    check("aktif").isInt({ min: 0, max: 1 }).withMessage("aktif harus berbentuk number 0 dan 1."),
    check("aktif").isLength({ min: 1,max:1 }).withMessage("aktif hanya 1 angka yang perlu diinput."),
  ];

  exports.updateAktifS2 = [
    check("kode_akun_2").notEmpty().withMessage("kode_akun_2 harus di isi."),
    check("kode_akun_2").isInt().withMessage("kode_akun_2 harus berbentuk number."),
    check("kode_akun_2").isLength({ min: 2,max:2 }).withMessage("kode_akun_2 hanya 2 angka yang perlu diinput."),
    check("aktif").notEmpty().withMessage("aktif harus di isi."),
    check("aktif").isInt({ min: 0, max: 1 }).withMessage("aktif harus berbentuk number 0 dan 1."),
    check("aktif").isLength({ min: 1,max:1 }).withMessage("aktif hanya 1 angka yang perlu diinput."),
  ];

  exports.updateAktifS3 = [
    check("kode_akun_3").notEmpty().withMessage("kode_akun_3 harus di isi."),
    check("kode_akun_3").isInt().withMessage("kode_akun_3 harus berbentuk number."),
    check("kode_akun_3").isLength({ min: 3,max:3 }).withMessage("kode_akun_3 hanya 3 angka yang perlu diinput."),
    check("aktif").notEmpty().withMessage("aktif harus di isi."),
    check("aktif").isInt({ min: 0, max: 1 }).withMessage("aktif harus berbentuk number 0 dan 1."),
    check("aktif").isLength({ min: 1,max:1 }).withMessage("aktif hanya 1 angka yang perlu diinput."),
  ];

  exports.updateAktifS4 = [
    check("kode_akun_4").notEmpty().withMessage("kode_akun_4 harus di isi."),
    check("kode_akun_4").isInt().withMessage("kode_akun_4 harus berbentuk number."),
    check("kode_akun_4").isLength({ min: 4,max:4 }).withMessage("kode_akun_4 hanya 4 angka yang perlu diinput."),
    check("aktif").notEmpty().withMessage("aktif harus di isi."),
    check("aktif").isInt({ min: 0, max: 1 }).withMessage("aktif harus berbentuk number 0 dan 1."),
    check("aktif").isLength({ min: 1,max:1 }).withMessage("aktif hanya 1 angka yang perlu diinput."),
  ];

  exports.updateAktifS5 = [
    check("kode_akun_5").notEmpty().withMessage("kode_akun_5 harus di isi."),
    check("kode_akun_5").isInt().withMessage("kode_akun_5 harus berbentuk number."),
    check("kode_akun_5").isLength({ min: 5,max:5 }).withMessage("kode_akun_5 hanya 5 angka yang perlu diinput."),
    check("aktif").notEmpty().withMessage("aktif harus di isi."),
    check("aktif").isInt({ min: 0, max: 1 }).withMessage("aktif harus berbentuk number 0 dan 1."),
    check("aktif").isLength({ min: 1,max:1 }).withMessage("aktif hanya 1 angka yang perlu diinput."),
  ];

  exports.updateAktifS6 = [
    check("kode_akun_6").notEmpty().withMessage("kode_akun_6 harus di isi."),
    check("kode_akun_6").isInt().withMessage("kode_akun_6 harus berbentuk number."),
    check("kode_akun_6").isLength({ min: 6,max:6 }).withMessage("kode_akun_6 hanya 6 angka yang perlu diinput."),
    check("aktif").notEmpty().withMessage("aktif harus di isi."),
    check("aktif").isInt({ min: 0, max: 1 }).withMessage("aktif harus berbentuk number 0 dan 1."),
    check("aktif").isLength({ min: 1,max:1 }).withMessage("aktif hanya 1 angka yang perlu diinput."),
  ];