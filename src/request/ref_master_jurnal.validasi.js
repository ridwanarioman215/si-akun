const { check } = require("express-validator");

exports.store = [
  check("kode_aplikasi").notEmpty().withMessage("kode_aplikasi harus di isi."),
  check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
  check("kode_modul").notEmpty().withMessage("kode_modul harus di isi."),
  check("modul").notEmpty().withMessage("modul harus di isi."),
  check("Debit").notEmpty().withMessage("Debit harus di isi."),
  check("Kredit").notEmpty().withMessage("Kredit harus di isi."),
  check("ucr").notEmpty().withMessage("ucr harus di isi.")
];

exports.update = [
    check("kode_aplikasi").notEmpty().withMessage("kode_aplikasi harus di isi."),
    check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
    check("kode_modul").notEmpty().withMessage("kode_modul harus di isi."),
    check("modul").notEmpty().withMessage("modul harus di isi."),
    check("Debit").notEmpty().withMessage("Debit harus di isi."),
    check("Kredit").notEmpty().withMessage("Kredit harus di isi."),
    check("uch").notEmpty().withMessage("uch harus di isi.")
  ];
