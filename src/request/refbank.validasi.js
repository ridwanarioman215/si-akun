const { check } = require("express-validator");

exports.createBank = [
  check("kode_bank").notEmpty().withMessage("kode_bank harus di isi."),
  check("nama_bank").notEmpty().withMessage("nama_bank harus di isi."),
  check("nomor_rekening").notEmpty().withMessage("nomor_rekening harus di isi."),
  check("kode_unit").notEmpty().withMessage("kode_unit harus di isi."),
  check("status_rekening").notEmpty().withMessage("status_rekening harus di isi. PUSAT/UNIT"),
  check("atas_nama_rekening").notEmpty().withMessage("atas_nama_rekening harus di isi."),
  check("alamat_bank").notEmpty().withMessage("alamat_bank harus di isi."),
  check("penanggung_jawab").notEmpty().withMessage("penanggung_jawab harus di isi."),
  check("kontak").notEmpty().withMessage("kontak harus di isi."),
  check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
  check("ucr").notEmpty().withMessage("ucr harus di isi.")
];

exports.updateBank = [
  check("kode_bank").notEmpty().withMessage("kode_bank harus di isi."),
  check("nama_bank").notEmpty().withMessage("nama_bank harus di isi."),
  check("atas_nama_rekening").notEmpty().withMessage("atas_nama_rekening harus di isi."),
  check("nomor_rekening").notEmpty().withMessage("nomor_rekening harus di isi."),
  check("kode_unit").notEmpty().withMessage("kode_unit harus di isi."),
  check("status_rekening").notEmpty().withMessage("status_rekening harus di isi. PUSAT/UNIT"),
  check("alamat_bank").notEmpty().withMessage("alamat_bank harus di isi."),
  check("penanggung_jawab").notEmpty().withMessage("penanggung_jawab harus di isi."),
  check("kontak").notEmpty().withMessage("kontak harus di isi."),
  check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
  check("uch").notEmpty().withMessage("uch harus di isi.")
];
