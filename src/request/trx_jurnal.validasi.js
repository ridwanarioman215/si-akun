const { check } = require("express-validator");

exports.createplanValidation = [
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("modul").notEmpty().withMessage("modul harus di isi."),
    check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
    check("kode_surat").notEmpty().withMessage("kode_surat harus di isi."),
    check("tanggal_transaksi").notEmpty().withMessage("tanggal_transaksi harus di isi."),
    check("jurnal_aktiva").notEmpty().withMessage("jurnal_aktiva harus di isi."),
    check("jurnal_pasiva").notEmpty().withMessage("jurnal_pasiva harus di isi."),
    check("akun_aktiva").notEmpty().withMessage("akun_aktiva harus di isi."),
    check("akun_pasiva").notEmpty().withMessage("akun_pasiva harus di isi."),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
    check("aktiva").notEmpty().withMessage("aktiva harus di isi."),
    check("pasiva").notEmpty().withMessage("pasiva harus di isi."),
    check("ucr").notEmpty().withMessage("ucr harus di isi.")
  ];

  exports.createreversalValidation = [
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("reversal").notEmpty().withMessage("reversal harus di isi."),
    check("modul").notEmpty().withMessage("modul harus di isi."),
    check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
    check("kode_surat").notEmpty().withMessage("kode_surat harus di isi."),
    check("tanggal_transaksi").notEmpty().withMessage("tanggal_transaksi harus di isi."),
    check("jurnal_aktiva").notEmpty().withMessage("jurnal_aktiva harus di isi."),
    check("jurnal_pasiva").notEmpty().withMessage("jurnal_pasiva harus di isi."),
    check("akun_aktiva").notEmpty().withMessage("akun_aktiva harus di isi."),
    check("akun_pasiva").notEmpty().withMessage("akun_pasiva harus di isi."),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi."),
    check("aktiva").notEmpty().withMessage("aktiva harus di isi."),
    check("pasiva").notEmpty().withMessage("pasiva harus di isi."),
    check("ucr").notEmpty().withMessage("ucr harus di isi.")
  ];

  exports.createplannewValidation = [
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("kode_aplikasi").notEmpty().withMessage("kode_aplikasi harus di isi."),
    check("kode_modul").notEmpty().withMessage("kode_modul harus di isi."),
    check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
    check("modul").notEmpty().withMessage("modul harus di isi."),
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("akun_bas").notEmpty().withMessage("akun_bas harus di isi"),
    check("kode_surat").notEmpty().withMessage("kode_surat harus di isi"),
    check("kode_sub_surat").notEmpty().withMessage("kode_sub_surat harus di isi"),
    check("tanggal_transaksi").notEmpty().withMessage("tanggal_transaksi harus di isi"),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi"),
    check("nominal").notEmpty().withMessage("nominal harus di isi"),
    check("kode_rkatu").notEmpty().withMessage("kode_rkatu harus di isi"),
    check("bulan_akhir").notEmpty().withMessage("bulan_akhir harus di isi"),
    check("ucr").notEmpty().withMessage("ucr harus di isi"),
  ];

  exports.createnewValidation = [
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("kode_aplikasi").notEmpty().withMessage("kode_aplikasi harus di isi."),
    check("kode_modul").notEmpty().withMessage("kode_modul harus di isi."),
    check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
    check("modul").notEmpty().withMessage("modul harus di isi."),
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("akun_bas").notEmpty().withMessage("akun_bas harus di isi"),
    check("kode_surat").notEmpty().withMessage("kode_surat harus di isi"),
    check("kode_sub_surat").notEmpty().withMessage("kode_sub_surat harus di isi"),
    check("norek_aktiva").notEmpty().withMessage("norek_aktiva harus di isi"),
    check("norek_pasiva").notEmpty().withMessage("norek_pasiva harus di isi"),
    check("tanggal_transaksi").notEmpty().withMessage("tanggal_transaksi harus di isi"),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi"),
    check("nominal").notEmpty().withMessage("nominal harus di isi"),
    check("kode_rkatu").notEmpty().withMessage("kode_rkatu harus di isi"),
    check("bulan_akhir").notEmpty().withMessage("bulan_akhir harus di isi"),
    check("remark").notEmpty().withMessage("remark harus di isi"),
    check("ucr").notEmpty().withMessage("ucr harus di isi"),
  ];

  exports.createnewreversalValidation = [
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("kode_aplikasi").notEmpty().withMessage("kode_aplikasi harus di isi."),
    check("kode_modul").notEmpty().withMessage("kode_modul harus di isi."),
    check("aplikasi").notEmpty().withMessage("aplikasi harus di isi."),
    check("modul").notEmpty().withMessage("modul harus di isi."),
    check("tahun").notEmpty().withMessage("tahun harus di isi."),
    check("akun_bas").notEmpty().withMessage("akun_bas harus di isi"),
    check("kode_surat").notEmpty().withMessage("kode_surat harus di isi"),
    check("kode_sub_surat").notEmpty().withMessage("kode_sub_surat harus di isi"),
    check("tanggal_transaksi").notEmpty().withMessage("tanggal_transaksi harus di isi"),
    check("keterangan").notEmpty().withMessage("keterangan harus di isi"),
    check("nominal").notEmpty().withMessage("nominal harus di isi"),
    check("kode_rkatu").notEmpty().withMessage("kode_rkatu harus di isi"),
    check("bulan_akhir").notEmpty().withMessage("bulan_akhir harus di isi"),
    check("ucr").notEmpty().withMessage("ucr harus di isi"),
  ]