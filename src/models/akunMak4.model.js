const { Sequelize } = require('sequelize');
const db = require('../config/database');
const akunMak5 = require('./akunMak5.model');


const { DataTypes } = Sequelize;

const akunMak4 = db.define('akunMak4', {
    kode_akun_4: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        allowNull: false
    },
    kode_akun_3: {
        type: DataTypes.STRING(11),
        allowNull: false
    },
    uraian_akun_4: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    aktif: {
        type: DataTypes.INTEGER(1),
        allowNull: false
    },
}, {
    tableName: 'ref_akun_4',
    createdAt: false,
    updatedAt: false,
})

akunMak4.hasMany(akunMak5, {
    foreignKey: "kode_akun_4",
});
akunMak5.belongsTo(akunMak4, {
    foreignKey: "kode_akun_4",
});


module.exports = akunMak4;