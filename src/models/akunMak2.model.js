const { Sequelize } = require('sequelize');
const db = require('../config/database');
const akunMak3 = require('./akunMak3.model');


const { DataTypes } = Sequelize;

const akunMak2 = db.define('akunMak2', {
    kode_akun_2: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        allowNull: false
    },
    kode_akun_1: {
        type: DataTypes.STRING(11),
        allowNull: false
    },
    uraian_akun_2: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    aktif: {
        type: DataTypes.INTEGER(1),
        allowNull: false
    },
}, {
    tableName: 'ref_akun_2',
    createdAt: false,
    updatedAt: false,
})

akunMak2.hasMany(akunMak3, {
    foreignKey: "kode_akun_2",
});
akunMak3.belongsTo(akunMak2, {
    foreignKey: "kode_akun_2",
});


module.exports = akunMak2;