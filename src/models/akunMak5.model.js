const { Sequelize } = require('sequelize');
const db = require('../config/database');
const akunMak = require('./akunMak.model');


const { DataTypes } = Sequelize;

const akunMak5 = db.define('akunMak5', {
    kode_akun_5: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        allowNull: false
    },
    kode_akun_4: {
        type: DataTypes.STRING(11),
        allowNull: false
    },
    uraian_akun_5: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    aktif: {
        type: DataTypes.INTEGER(1),
        allowNull: false
    },
}, {
    tableName: 'ref_akun_5',
    createdAt: false,
    updatedAt: false,
})

akunMak5.hasMany(akunMak, {
    foreignKey: "kode_akun_5",
});
akunMak.belongsTo(akunMak5, {
    foreignKey: "kode_akun_5",
});


module.exports = akunMak5;