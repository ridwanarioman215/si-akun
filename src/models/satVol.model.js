const { Sequelize } = require('sequelize');
const db = require('../config/database');
const satSbm = require('./satsbm.model');

const { DataTypes } = Sequelize;

const satVolume = db.define('satVolume', {
    id_satuan: {
        type: DataTypes.STRING(11),
        primaryKey:true,
        allowNull: false
    },
    id_satvol: {
        type: DataTypes.STRING(2),
        primaryKey: true,
        allowNull: false
    },
    sat_vol1: {
        type: DataTypes.STRING(4),
        allowNull: false
    },sat_vol2: {
        type: DataTypes.STRING(4),
        allowNull: false
    },sat_vol3: {
        type: DataTypes.STRING(4),
        allowNull: false
    },sat_vol4: {
        type: DataTypes.STRING(4),
        allowNull: false
    },
    jumlah_sbm: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    
}, {
    tableName: 'ref_sbm',
    createdAt: false,
    updatedAt: false,
})

satSbm.hasMany(satVolume, {
    foreignKey: "id_satvol",
});
satVolume.belongsTo(satSbm, {
    foreignKey: "id_satvol",
});

module.exports = satVolume;