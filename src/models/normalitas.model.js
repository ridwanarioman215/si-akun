const { Sequelize } = require('sequelize');
const db = require('../config/database');


const { DataTypes } = Sequelize;

const normalitas = db.define('refnormalitas', {
    kode_normalitas: {
        type: DataTypes.CHAR(3),
        primaryKey: true,
        allowNull: false
    },
    uraian_normalitas: {
        type: DataTypes.CHAR(50),
        allowNull: false
    },
}, {
    tableName: 'ref_normalitas',
    createdAt: false,
    updatedAt: false,
})


module.exports = normalitas;