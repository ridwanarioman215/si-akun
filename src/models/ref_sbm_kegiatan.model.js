const { Sequelize } = require('sequelize');
const db = require('../config/database');
// const akunMak = require('./akunMak.model');
const akunMak = require('./akunMak.model');
const satuanKegiatan = require(`./ref_satuan.model`);

const { DataTypes } = Sequelize;

const sbmKegiatan= db.define('sbmKeg', {
    kode_sbm: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        autoIncrement:true
    },
    jumlah_sbm: {
        type: DataTypes.BIGINT(25),
        allowNull: false,
        autoIncrement:true   
    },
    kode_akun_6: {
        type: DataTypes.STRING(7),
        allowNull: false
    },
    kode_satuan: {
        type: DataTypes.INTEGER(11),
        primaryKey:true,
        allowNull: false
    },
    jumlah_sbm: {
        type: DataTypes.BIGINT(25),
        allowNull: false,   
    },
},{
    tableName: 'ref_sbm_kegiatan',
    createdAt: false,
    updatedAt: false,
})


akunMak.hasMany(sbmKegiatan, {
    foreignKey: "kode_akun_6",
});
sbmKegiatan.belongsTo(akunMak, {
    foreignKey: "kode_akun_6",
});

satuanKegiatan.hasMany(sbmKegiatan, {
    foreignKey: "kode_satuan",
});
sbmKegiatan.belongsTo(satuanKegiatan, {
    foreignKey: "kode_satuan",
});

module.exports = sbmKegiatan;
