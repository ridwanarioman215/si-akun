const { Sequelize } = require('sequelize');
const db = require('../config/database');
const satuanKeg = require('./satuanKeg.model');
const normalitas = require('./normalitas.model');


const { DataTypes } = Sequelize;

const akunMak = db.define('akunMak', {
    kode_akun_6: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        allowNull: false
    },
    kode_akun_5: {
        type: DataTypes.STRING(7),
        allowNull: false
    },
    uraian_akun_6: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    aktif: {
        type: DataTypes.INTEGER(1),
        allowNull: false
    },
    keterangan: {
        type: DataTypes.TEXT,
        allowNull: false
    },
}, {
    tableName: 'ref_akun_6',
    createdAt: false,
    updatedAt: false,
})

akunMak.hasMany(satuanKeg, {
    foreignKey: "kode_akun_6",
});
satuanKeg.belongsTo(akunMak, {
    foreignKey: "kode_akun_6",
});


module.exports = akunMak;