const { Sequelize } = require('sequelize');
const db = require('../config/database');
// const akunMak = require('./akunMak.model');
const satVolume = require('./satVol.model');

const { DataTypes } = Sequelize;

const satuanKeg= db.define('satuanKeg', {
    kode_akun_6: {
        type: DataTypes.STRING(7),
        allowNull: false
    },
    id_satuan: {
        type: DataTypes.STRING(11),
        primaryKey:true,
        allowNull: false
    },
    nama_satuan: {
        type: DataTypes.STRING(255),
        allowNull: false,   
    },
},{
    tableName: 'ref_satuan_keg',
    createdAt: false,
    updatedAt: false,
})


satVolume.hasMany(satuanKeg, {
    foreignKey: "id_satuan",
});
satuanKeg.belongsTo(satVolume, {
    foreignKey: "id_satuan",
});

module.exports = satuanKeg;
