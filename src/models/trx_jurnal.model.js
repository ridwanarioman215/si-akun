const { Sequelize } = require('sequelize');
const db = require('../config/database');
const akunMak = require('./akunMak.model')

const { DataTypes } = Sequelize;

const trxJurnal = db.define('trxJurnal', {
    kode_transaksi: {
        type: DataTypes.BIGINT(25),
        primaryKey: true,
        allowNull: true
    },
    reversal: {
        type: DataTypes.BIGINT(25),
        allowNull: true
    },
    realisasi: {
        type: DataTypes.BIGINT(25),
        allowNull: true
    },
    tanggal_transaksi: {
        type: DataTypes.DATE,
        allowNull: false
    },
    keterangan: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    modul: {
        type: DataTypes.CHAR(50),
        allowNull: true
    },
    aplikasi: {
        type: DataTypes.CHAR(50),
        allowNull: true
    },
    kode_surat: {
        type: DataTypes.CHAR(50),
        allowNull: true
    },
    kode_sub_surat: {
        type: DataTypes.CHAR(50),
        allowNull: true
    },
    jurnal_aktiva: {
        type: DataTypes.STRING(225),
        allowNull: true
    },
    jurnal_pasiva: {
        type: DataTypes.STRING(225),
        allowNull: true
    },
    akun_aktiva: {
        type: DataTypes.CHAR(7),
        allowNull: false
    },
    akun_pasiva: {
        type: DataTypes.CHAR(7),
        allowNull: false
    },
    norek_aktiva: {
        type: DataTypes.CHAR(100),
        allowNull: true
    },
    norek_pasiva: {
        type: DataTypes.CHAR(100),
        allowNull: true
    },
    Aktiva: {
        type: DataTypes.BIGINT(25),
        allowNull: false
    },
    Pasiva: {
        type: DataTypes.BIGINT(25),
        allowNull: false
    },
    kode_rkatu:{
        type: DataTypes.INTEGER(11),
        allowNull: true
    },
    bulan_akhir:{
        type: DataTypes.INTEGER(11),
        allowNull: true
    },
    tahun_rkatu:{
        type: DataTypes.INTEGER(4),
        allowNull: true
    },
    ucr: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    uch: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    udcr: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    udch: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    
}, {
    tableName: 'trx_jurnal',
    createdAt: 'udcr',
    updatedAt: 'udch',
})

trxJurnal.belongsTo(akunMak, {
    foreignKey: "akun_pasiva",targetKey: "kode_akun_6",attribute:'uraian_akun_6', as:'jurnal_pasiva2'
});
trxJurnal.belongsTo(akunMak, {
    foreignKey: "akun_aktiva",targetKey: "kode_akun_6",attribute:'uraian_akun_6', as:'jurnal_aktiva2'
});

module.exports = trxJurnal;