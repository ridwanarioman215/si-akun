const { Sequelize } = require('sequelize');
const db = require('../config/database');
const akunMak4 = require('./akunMak4.model');


const { DataTypes } = Sequelize;

const akunMak3 = db.define('akunMak3', {
    kode_akun_3: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        allowNull: false
    },
    kode_akun_2: {
        type: DataTypes.STRING(11),
        allowNull: false
    },
    uraian_akun_3: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    aktif: {
        type: DataTypes.INTEGER(1),
        allowNull: false
    },
}, {
    tableName: 'ref_akun_3',
    createdAt: false,
    updatedAt: false,
})

akunMak3.hasMany(akunMak4, {
    foreignKey: "kode_akun_3",
});
akunMak4.belongsTo(akunMak3, {
    foreignKey: "kode_akun_3",
});


module.exports = akunMak3;