const { Sequelize } = require('sequelize');
const db = require('../config/database');
const { DataTypes } = Sequelize;
const akunMak = require('./akunMak.model')

const refMasterJurnal= db.define('penyeimbangJurnal', {
    kode_penyeimbang: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement:true
    },
    keterangan: {
        type: DataTypes.STRING(225),
        allowNull: false
    },
    kode_aplikasi: {
        type: DataTypes.STRING(5),
        allowNull: false
    },
    aplikasi: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    kode_modul: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    modul: {
        type: DataTypes.STRING(50),
        allowNull: false,   
    },
    Debit: {
        type: DataTypes.STRING(7),
        allowNull: false,   
    },
    Kredit: {
        type: DataTypes.STRING(7),
        allowNull: false,   
    },
    ucr: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      uch: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      udcr: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      udch: {
        type: DataTypes.DATE,
        allowNull: true,
      },
},{
    tableName: 'ref_master_jurnal',
    createdAt: 'udcr',
    updatedAt: 'udch',
})

refMasterJurnal.belongsTo(akunMak, {
    foreignKey: "Debit",targetKey: "kode_akun_6",attribute:'uraian_akun_6', as:'jurnal_Debit2'
});
refMasterJurnal.belongsTo(akunMak, {
    foreignKey: "Kredit",targetKey: "kode_akun_6",attribute:'uraian_akun_6', as:'jurnal_Kredit2'
});


module.exports = refMasterJurnal;
