const { Sequelize } = require('sequelize');
const db = require('../config/database');
// const satuanKeg = require('./satuanKeg.model');

const { DataTypes } = Sequelize;

const satuanBiaya = db.define('satuanBiaya', {
    id_satvol: {
        type: DataTypes.STRING(2),
        primaryKey: true,
        allowNull: false
    },
    id_sbm: {
        type: DataTypes.STRING(11),
        primaryKey:true,
        allowNull: false
    },
    jumlah_sbm: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    
}, {
    tableName: 'ref_sbm',
    createdAt: false,
    updatedAt: false,
})



module.exports = satuanBiaya;