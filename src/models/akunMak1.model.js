const { Sequelize } = require('sequelize');
const db = require('../config/database');
const akunMak2 = require('./akunMak2.model');
const normalitas = require('./normalitas.model');


const { DataTypes } = Sequelize;

const akunMak1 = db.define('akunMak1', {
    kode_akun_1: {
        type: DataTypes.STRING(11),
        primaryKey: true,
        allowNull: false
    },
    uraian_akun_1: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    normalitas: {
        type: DataTypes.CHAR(3),
        allowNull: false
    },
    aktif: {
        type: DataTypes.INTEGER(1),
        allowNull: false
    },
}, {
    tableName: 'ref_akun_1',
    createdAt: false,
    updatedAt: false,
})

akunMak1.hasMany(akunMak2, {
    foreignKey: "kode_akun_1",
});
akunMak2.belongsTo(akunMak1, {
    foreignKey: "kode_akun_1",
});
akunMak1.belongsTo(normalitas, {
    foreignKey: "normalitas",targetKey:"kode_normalitas"
});


module.exports = akunMak1;