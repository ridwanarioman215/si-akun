const { Sequelize } = require('sequelize');
const db = require('../config/database');

const akunMak = require('./akunMak.model');
const satuanKegiatan = require(`./ref_satuan.model`);
const bentukKegiatan = require(`./refbentukkegiatan.model`);

const { DataTypes } = Sequelize;

const sbmakun= db.define('sbmakun', {
    kode_sbm: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement:true
    },
    kode_akun_6: {
        type: DataTypes.STRING(7),
        allowNull: false
    },
    kode_satuan: {
        type: DataTypes.INTEGER(11),
        allowNull: false
    },
    kode_bentuk_kegiatan: {
        type: DataTypes.INTEGER(11),
        allowNull: false,   
    },
    komponen: {
        type: DataTypes.STRING(225),
        allowNull: false,   
    },
    keterangan: {
        type: DataTypes.TEXT,
        allowNull: false,   
    },
    satuan_biaya: {
        type: DataTypes.BIGINT(25),
        allowNull: false,   
    },
    ucr: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      uch: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      udcr: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      udch: {
        type: DataTypes.DATE,
        allowNull: true,
      },
},{
    tableName: 'ref_sbm_akun_bas',
    createdAt: 'udcr',
    updatedAt: 'udch',
})


akunMak.hasMany(sbmakun, {
    foreignKey: "kode_akun_6",
});
sbmakun.belongsTo(akunMak, {
    foreignKey: "kode_akun_6",
});

satuanKegiatan.hasMany(sbmakun, {
    foreignKey: "kode_satuan",
});
sbmakun.belongsTo(satuanKegiatan, {
    foreignKey: "kode_satuan",
});

bentukKegiatan.hasMany(sbmakun, {
    foreignKey: "kode_bentuk_kegiatan",
});
sbmakun.belongsTo(bentukKegiatan, {
    foreignKey: "kode_bentuk_kegiatan",
});

module.exports = sbmakun;
