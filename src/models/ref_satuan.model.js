const { Sequelize } = require('sequelize');
const db = require('../config/database');
// const satuanKeg = require('./satuanKeg.model');

const { DataTypes } = Sequelize;

const satuanKegiatan = db.define('satuanKegiatan', {
    kode_satuan: {
        type: DataTypes.INTEGER(11),
        primaryKey: true,
        allowNull: true
    },
    nama_satuan: {
        type: DataTypes.STRING(100),
        primaryKey:true,
        allowNull: false
    },
    uraian: {
        type: DataTypes.STRING(100),
        primaryKey:true,
        allowNull: false
    },
    
}, {
    tableName: 'ref_satuan',
    createdAt: false,
    updatedAt: false,
})



module.exports = satuanKegiatan;