const { Sequelize } = require('sequelize');
const db = require('../config/database');

const { DataTypes } = Sequelize;

const refBank = db.define('refBank', {
    kode_bank: {
        type: DataTypes.CHAR(3),
        primaryKey: true,
        allowNull: false
    },
    nomor_rekening: {
        type: DataTypes.CHAR(50),
        primaryKey: true,
        allowNull: false
        
    },
    kode_unit: {
        type: DataTypes.CHAR(25),
        primaryKey: true,
        allowNull: false
    },
    nama_bank: {
        type: DataTypes.CHAR(100),
        allowNull: false
    },
    atas_nama_rekening: {
        type: DataTypes.CHAR(100),
        allowNull: false
    },
    alamat_bank: {
        type: DataTypes.STRING(225),
        allowNull: false
    },
    penanggung_jawab:{
        type: DataTypes.CHAR(50),
        allowNull: false
    },
    kontak:{
        type: DataTypes.CHAR(20),
        allowNull: false
    },
    keterangan:{
        type: DataTypes.CHAR(100),
        allowNull: false
    },
    dokumen:{
        type: DataTypes.STRING(225),
        allowNull: false
    },
    status_rekening:{
        type: DataTypes.CHAR(25),
        allowNull:false
    },
    aktif:{
        type: DataTypes.INTEGER(1),
        allowNull:false
    },
    ucr: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      uch: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      udcr: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      udch: {
        type: DataTypes.DATE,
        allowNull: true,
      },
}, {
    tableName: 'ref_bank',
    createdAt: 'udcr',
    updatedAt: 'udch',
})



module.exports = refBank;