const { Sequelize } = require('sequelize');
const db = require('../config/database');


const { DataTypes } = Sequelize;

const bentukKegiatan = db.define('bentukegiatan', {
    kode_bentuk_kegiatan: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement:true   
    },
    uraian: {
        type: DataTypes.TEXT,
        allowNull: false,   
    },
    ucr: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      uch: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      udcr: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      udch: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    
},{
    tableName: 'ref_bentuk_kegiatan',
    createdAt: 'udcr',
    updatedAt: 'udch',
})



module.exports = bentukKegiatan;
