const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const { QueryTypes,Op,fn,col } = require("sequelize");
const refMasterJurnal = require("../models/ref_master_jurnal.model");

exports.index = async(req,res,next)=>{
    try{

     let master = await refMasterJurnal.findAll().then((data)=>{
        if(data.length == 0){
            let err = new Error("data master Tidak Ada Di database")
            throw err
        }
        return data
     }).catch((err)=>{throw err})

     return jsonFormat(res,"success","Berhasil menampilkan Data", master)
    }catch(err){
    return next(err)
    }
}

exports.show = async(req,res,next)=>{
    try{

     let master = await refMasterJurnal.findOne({where:{kode_penyeimbang:req.params.kode_penyeimbang}}).then((data)=>{
        if(data == null){
            let err = new Error("data master Tidak Ada Di database")
            throw err
        }
        return data
     }).catch((err)=>{throw err})

     return jsonFormat(res,"success","Berhasil menampilkan Data", master)
    }catch(err){
    return next(err)
    }
}

exports.showaplikasi = async(req,res,next)=>{
    try{

     let master = await refMasterJurnal.findAll({where:{kode_aplikasi:req.params.kode_aplikasi}}).then((data)=>{
        if(data.length == 0){
            let err = new Error("data master Tidak Ada Di database")
            throw err
        }
        return data
     }).catch((err)=>{throw err})

     return jsonFormat(res,"success","Berhasil menampilkan Data", master)
    }catch(err){
    return next(err)
    }
}

exports.store = async(req,res,next) =>{
 try{

    let cek = await refMasterJurnal.findAll({where:{
        kode_aplikasi:req.body.kode_aplikasi,
        kode_modul:req.body.kode_modul,
        Debit:req.body.Debit
    }}).then((data)=>{
        if(data.length > 0){
            let err = new Error('Data Telah Ada Di Database')
            err.statusCode = 401
            throw err
        }
        return data
    }).catch((err)=>{throw err})

    let master = await refMasterJurnal.create({
    keterangan:req.body.keterangan,
    kode_aplikasi:req.body.kode_aplikasi,
    aplikasi:req.body.aplikasi,
    kode_modul:req.body.kode_modul,
    modul:req.body.modul,
    Debit:req.body.Debit,
    Kredit:req.body.Kredit,
    ucr:req.body.ucr
}).catch((err)=>{throw err})

return jsonFormat(res,"success","Berhasil menambahkan Data", master)

 }catch(err){
 return next(err)
 } 
}

exports.update = async(req,res,next) =>{
    try{
   let master = await refMasterJurnal.update({
       keterangan:req.body.keterangan,
       kode_aplikasi:req.body.kode_aplikasi,
       aplikasi:req.body.aplikasi,
       kode_modul:req.body.kode_modul,
       modul:req.body.modul,
       Debit:req.body.Debit,
       Kredit:req.body.Kredit,
       uch:req.body.uch
   },{where:{kode_penyeimbang:req.params.kode_penyeimbang}})
   .then((data)=>{
    if(data == 0){
        let err = new Error("Tidak Ada data yang di update")
        err.statusCode = 401
        throw err
    }
    return data
   })
   .catch((err)=>{throw err})
   
   return jsonFormat(res,"success","Berhasil merubah Data", master)
   
    }catch(err){
    return next(err)
    } 
   }

exports.hapus = async(req,res,next) =>{
    try{
    let master = await refMasterJurnal.destroy({where:{kode_penyeimbang:req.params.kode_penyeimbang}})
    .then((data)=>{
        if(data == 0){
            let err = new Error("Tidak Ada data yang di update")
            err.statusCode = 401
            throw err
        }
        return data
       })
       .catch((err)=>{throw err})
    }catch(err){
        return next(err)
    }
}