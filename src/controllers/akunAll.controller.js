const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const shape = require('shape-json');
const { QueryTypes, Sequelize } = require('sequelize');

exports.getAllAkunNest = async (req, res, next) => {
    try {
        const result = await db.query(
            `SELECT satu.kode_akun_1, satu.uraian_akun_1,
            dua.kode_akun_2, dua.uraian_akun_2,
            tiga.kode_akun_3, tiga.uraian_akun_3,
            empat.kode_akun_4, empat.uraian_akun_4,
            lima.kode_akun_5, lima.uraian_akun_5,
            enam.kode_akun_6, enam.uraian_akun_6
            FROM ref_akun_6 AS enam
            LEFT JOIN ref_akun_5 AS lima
            ON enam.kode_akun_5 = lima.kode_akun_5
            LEFT JOIN ref_akun_4 AS empat
            ON lima.kode_akun_4 = empat.kode_akun_4
            LEFT JOIN ref_akun_3 AS tiga
            ON empat.kode_akun_3 = tiga.kode_akun_3
            LEFT JOIN ref_akun_2 AS dua
            ON tiga.kode_akun_2 = dua.kode_akun_2
            LEFT JOIN ref_akun_1 AS satu
            ON dua.kode_akun_1 = satu.kode_akun_1 
            GROUP BY satu.kode_akun_1, dua.kode_akun_2, tiga.kode_akun_3, empat.kode_akun_4, lima.kode_akun_5, enam.kode_akun_6`,
            {
                type: Sequelize.QueryTypes.SELECT
            }
        );
        var input = result;
        var scheme = {
            "$group[Akun1](kode_akun_1)": {
                "kode_akun_1": "kode_akun_1",
                "uraian_akun_1": "uraian_akun_1",
                "$group[akun2](kode_akun_2)": {
                    "kode_akun_2": "kode_akun_2",
                    "uraian_akun_2": "uraian_akun_2",
                    "$group[akun3](kode_akun_3)": {
                        "kode_akun_3": "kode_akun_3",
                        "uraian_akun_3": "uraian_akun_3",
                        "$group[akun4](kode_akun_4)": {
                            "kode_akun_4": "kode_akun_4",
                            "uraian_akun_4": "uraian_akun_4",
                            "$group[akun5](kode_akun_5)": {
                                "kode_akun_5": "kode_akun_5",
                                "uraian_akun_5": "uraian_akun_5",
                                "$group[akun6](kode_akun_6)": {
                                    "kode_akun_6": "kode_akun_6",
                                    "uraian_akun_6": "uraian_akun_6",
                                }
                            }
                        }
                    }
                }
            }
        }
        console.log(shape.parse(input, scheme));
        res.json(shape.parse(input, scheme), "success", "Berhasil memuat standar akun mak");
    } catch (error) {
        jsonFormat(res, "failed", error.message, []);
    }
};

exports.getAllAkun = async (req, res, next) => {
    try {
        const result = await db.query(
            `SELECT satu.kode_akun_1, satu.uraian_akun_1,
            dua.kode_akun_2, dua.uraian_akun_2,
            tiga.kode_akun_3, tiga.uraian_akun_3,
            empat.kode_akun_4, empat.uraian_akun_4,
            lima.kode_akun_5, lima.uraian_akun_5,
            enam.kode_akun_6, enam.uraian_akun_6
            FROM ref_akun_6 AS enam
            LEFT JOIN ref_akun_5 AS lima
            ON enam.kode_akun_5 = lima.kode_akun_5
            LEFT JOIN ref_akun_4 AS empat
            ON lima.kode_akun_4 = empat.kode_akun_4
            LEFT JOIN ref_akun_3 AS tiga
            ON empat.kode_akun_3 = tiga.kode_akun_3
            LEFT JOIN ref_akun_2 AS dua
            ON tiga.kode_akun_2 = dua.kode_akun_2
            LEFT JOIN ref_akun_1 AS satu
            ON dua.kode_akun_1 = satu.kode_akun_1 
            GROUP BY satu.kode_akun_1, dua.kode_akun_2, tiga.kode_akun_3, empat.kode_akun_4, lima.kode_akun_5, enam.kode_akun_6`,
            {
                type: Sequelize.QueryTypes.SELECT
            }
        );
        jsonFormat(res, "success", "Berhasil memuat standar akun mak", result);
    } catch (error) {
        jsonFormat(res, "failed", error.message, []);
    }
};
