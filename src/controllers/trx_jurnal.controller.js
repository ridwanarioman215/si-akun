const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const { validationResult } = require("express-validator");
const trxJurnal = require("../models/trx_jurnal.model");
const refMasterJurnal = require("../models/ref_master_jurnal.model");
const { QueryTypes,Op,fn,col,literal } = require("sequelize");
const { ReadableStreamBYOBRequest } = require("stream/web");
const angkaKali = 10000000000

exports.nestedJurnal = async(req,res,next)=>{
 
    trxJurnal.findAll({group:'tanggal_transaksi'})
    .then((tgt)=>{
        if(tgt.length === 0){
            res.statusCode = 404;
            let err = new error('Data tidak ada')
            throw err
        }
        trxJurnal.findAll({group:['tanggal_transaksi','kode_transaksi']})
        .then((ttk)=>{
            trxJurnal.findAll().then((t)=>{
                let arrtgt = []
                tgt.map((tgtm)=>{
                    let arrttk = []
                    let Debitarrttk = 0;
                    let Kreditarrttk = 0;
                    ttk.map((ttkm)=>{
                        if(tgtm.tanggal_transaksi===ttkm.tanggal_transaksi){
                            let arrt = []
                            let Debitarrt = 0;
                            let Kreditarrt = 0;
                            t.map((tm)=>{
                                if(tm.tanggal_transaksi===ttkm.tanggal_transaksi && tm.kode_transaksi===ttkm.kode_transaksi){
                                    Debitarrt = Debitarrt+tm.Aktiva
                                    Kreditarrt = Kreditarrt+tm.Pasiva
                                    arrt.push({
                                        "kode_katagori":tm.kode_katagori,
                                        "kode_katagori_sub":tm.kode_katagori_sub,
                                        "modul":tm.modul,
                                        "aplikasi":tm.aplikasi,
                                        "kode_surat":tm.kode_surat,
                                        "akun":tm.akun,
                                        "keterangan":tm.keterangan,
                                        "aktiva":tm.Aktiva,
                                        "pasiva":tm.Pasiva
                                    })
                                }
                            })
                            Debitarrttk = Debitarrttk+Debitarrt
                            Kreditarrttk = Kreditarrttk+Kreditarrt
                            arrttk.push({
                                "kode_transaksi":ttkm.kode_transaksi,
                                "Aktiva":Debitarrt,
                                "Pasiva":Kreditarrt,
                                "perakun":arrt
                            })
                        }
                    })
                    arrtgt.push({
                        "tanggal_transaksi":tgtm.tanggal_transaksi,
                        "Aktiva":Debitarrttk,
                        "pasiva":Kreditarrttk,
                        "perkode":arrttk
                    })
                })
                jsonFormat(res, "success", "berhasil menampilkan daata", arrtgt)
            })
        })
    })
    .catch((error)=>{
        jsonFormat(res, "failed", error.message, []);
    })
}

exports.nestedNewJurnal = async(req,res,nex)=>{
    trxJurnal.findAll({where:{'tanggal_transaksi':{
        [Op.between]: [req.params.tanggal_awal , req.params.tanggal_akhir ]
      }},group:['tanggal_transaksi','kode_katagori','kode_transaksi']})
    .then((induk)=>{
        if(induk.length === 0){
            let error = new Error('Data tidak ditemukan')
             throw error
        }
        return trxJurnal.findAll({where:{'tanggal_transaksi':{
            [Op.between]: [req.params.tanggal_awal , req.params.tanggal_akhir ]
          }}}).then((anak)=>{
            let arrinduk = []
            induk.map((i)=>{
                let arranak = []
                anak.map((a)=>{
                    if(a.tanggal_transaksi == i.tanggal_transaksi && a.kode_katagori == i.kode_katagori
                    && a.kode_transaksi == i.kode_transaksi){
                            arranak.push({
                                kode_katagori_sub:a.kode_katagori_sub,
                                modul:a.modul,
                                aplikasi:a.aplikasi,
                                kode_surat:a.kode_surat,
                                akun:a.akun,
                                keterangan:a.keterangan,
                                aktiva:a.Aktiva,
                                pasiva:a.Pasiva
                            })
                        }
                })
                arrinduk.push({
                    tanggal_transaksi:i.tanggal_transaksi,
                    kode_katagori:i.kode_katagori,
                    kode_transaksi:i.kode_transaksi,
                    arranak1: arranak
                })
            })
            jsonFormat(res, "success", "berhasil menampilkan daata", arrinduk)
        })
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.filterJurnal = async(req,res,nex)=>{
    trxJurnal.findAll({where:{'tanggal_transaksi':{
        [Op.between]: [req.params.tanggal_awal , req.params.tanggal_akhir ]
      }},include:['jurnal_aktiva2','jurnal_pasiva2']}).then((data)=>{
        jsonFormat(res, "success", "berhasil menampilkan daata", data)
      }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.jurnalById = async(req,res,nex)=>{
    await trxJurnal.findOne({where:{kode_transaksi:req.params.kode_transaksi}}).then((data)=>{
        if(data == null){
            let error = new Error('Data tidak ditemukan')
            throw error
        }
        jsonFormat(res, "success", "berhasil menampilkan daata", data)
      }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.createplanpengeluaran = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }

    //buat kode transaksi baru
    const tahunMax = await trxJurnal.findAll({ attributes: [[fn('max', col('kode_transaksi')), 'maxKodeTransaksi']],
    raw: true,where:{kode_transaksi:{[Op.like]: req.body.tahun+'%' }}});
    let intditahun,MaxdiTahun
    const maxKodeTransaksi = tahunMax[0].maxKodeTransaksi;
     if(maxKodeTransaksi == null){
        MaxdiTahun = "1"
    }
    else{
        const strMaxTransaksi = maxKodeTransaksi.toString()
        intditahun = parseInt(strMaxTransaksi.substr(4, 25))+1
        MaxdiTahun = intditahun.toString()

    }
    const strTahun = req.body.tahun.toString()
    let kodeTransaksiNew = strTahun+MaxdiTahun
    // input data
    await trxJurnal.create({
            kode_transaksi:kodeTransaksiNew,
            tanggal_transaksi:req.body.tanggal_transaksi,
            keterangan:req.body.keterangan,
            modul:req.body.modul,
            aplikasi:req.body.aplikasi,
            kode_surat:req.body.kode_surat,
            jurnal_aktiva:req.body.jurnal_aktiva,
            jurnal_pasiva:req.body.jurnal_pasiva,
            akun_aktiva:req.body.akun_aktiva,
            akun_pasiva:req.body.akun_pasiva,
            Aktiva:req.body.aktiva,
            Pasiva:req.body.pasiva,
            kode_rkatu:req.body.kode_rkatu,
            bulan_akhir:req.body.bulan_akhir,
            tahun_rkatu:req.body.tahun_rkatu,
            ucr:req.body.ucr
    }).then((create)=>{
        jsonFormat(res, "success", "berhasil menampilkan daata", create)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.createpengeluaran = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }

    //buat kode transaksi baru
    const tahunMax = await trxJurnal.findAll({ attributes: [[fn('max', col('kode_transaksi')), 'maxKodeTransaksi']],
    raw: true,where:{kode_transaksi:{[Op.like]: req.body.tahun+'%' }}});
    let intditahun,MaxdiTahun
    const maxKodeTransaksi = tahunMax[0].maxKodeTransaksi;
     if(maxKodeTransaksi == null){
        MaxdiTahun = "1"
    }
    else{
        const strMaxTransaksi = maxKodeTransaksi.toString()
        intditahun = parseInt(strMaxTransaksi.substr(4, 25))+1
        MaxdiTahun = intditahun.toString()

    }
    const strTahun = req.body.tahun.toString()
    let dataJurnal = []
    let kodeTransaksiNew = strTahun+MaxdiTahun
    for(let a = 0; a < req.body.jurnal.length ; a++){
            let kode_transaksi = kodeTransaksiNew+a
            let tanggal_transaksi = req.body.jurnal[a].tanggal_transaksi
            let keterangan=req.body.jurnal[a].keterangan
            let modul=req.body.jurnal[a].modul
            let aplikasi=req.body.jurnal[a].aplikasi
            let kode_surat=req.body.jurnal[a].kode_surat
            let jurnal_aktiva=req.body.jurnal[a].jurnal_aktiva
            let jurnal_pasiva=req.body.jurnal[a].jurnal_pasiva
            let akun_aktiva=req.body.jurnal[a].akun_aktiva
            let akun_pasiva=req.body.jurnal[a].akun_pasiva
            let Aktiva=req.body.jurnal[a].Aktiva
            let Pasiva=req.body.jurnal[a].Pasiva
            let kode_rkatu = req.body[a].kode_rkatu
            let bulan_akhir = req.body[a].bulan_akhir
            let tahun_rkatu = req.body[a].tahun_rkatu
            let ucr=req.body.jurnal[a].ucr

            dataJurnal.push({
            kode_transaksi:kode_transaksi,
            tanggal_transaksi:tanggal_transaksi,
            keterangan:keterangan,
            modul:modul,
            aplikasi:aplikasi,
            kode_surat:kode_surat,
            jurnal_aktiva:jurnal_aktiva,
            jurnal_pasiva:jurnal_pasiva,
            akun_aktiva:akun_aktiva,
            akun_pasiva:akun_pasiva,
            Aktiva:Aktiva,
            Pasiva:Pasiva,
            kode_rkatu:kode_rkatu,
            bulan_akhir:bulan_akhir,
            tahun_rkatu:tahun_rkatu,
            ucr:ucr
        })
    }
    
    // input data
    await trxJurnal.bulkCreate(dataJurnal).then((create)=>{
        jsonFormat(res, "success", "berhasil menampilkan daata", create)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.createreversal = async(req,res,next)=>{
     const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }

    //buat kode transaksi baru
    let newKodeTransaksi = await trxJurnal.max('kode_transaksi',{where:{tahun_rkatu:req.body.tahun}}).then((kodMax)=>{
        if(kodMax == null){
            kodMax = req.body.tahun*angkaKali
        }
            return kodMax+1
    }).catch((err)=>{throw err})
    
    await trxJurnal.findAll({where:{kode_transaksi:req.body.reversal,reversal:0}}).then((data)=>{
        if(data.length == 0){
            let error = new Error('Data transaksi yang ingin direversal tidak ditemukan')
            throw error
        }
        return trxJurnal.findAll({where:{reversal:req.body.reversal,reversal:0}}).then((dataanak)=>{
            if(dataanak.length > 0){
                let error = new Error('Data transaksi yang sudah direversal tidak direversal kembali')
                throw error
            } 
            return trxJurnal.create({
                kode_transaksi:newKodeTransaksi,
                reversal:req.body.reversal,
                tanggal_transaksi:req.body.tanggal_transaksi,
                keterangan:req.body.keterangan+"-Reversal",
                modul:req.body.modul,
                aplikasi:req.body.aplikasi,
                kode_surat:req.body.kode_surat,
                jurnal_aktiva:req.body.jurnal_aktiva,
                jurnal_pasiva:req.body.jurnal_pasiva,
                akun_aktiva:req.body.akun_aktiva,
                akun_pasiva:req.body.akun_pasiva,
                Aktiva:req.body.aktiva,
                Pasiva:req.body.pasiva,
                kode_rkatu:req.body.kode_rkatu,
                bulan_akhir:req.body.bulan_akhir,
                tahun_rkatu:req.body.tahun_rkatu,
                ucr:req.body.ucr
        }).then((create)=>{
            jsonFormat(res, "success", "berhasil menampilkan daata", create)
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
        
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
    
}

exports.storePlanExpend = async(req,res,next)=>{ 
    try{
        let penyeimbang = await refMasterJurnal.findOne({include:['jurnal_Debit2','jurnal_Kredit2'],where:{kode_aplikasi:req.body.kode_aplikasi,kode_modul:req.body.kode_modul,Debit:req.body.akun_bas}}).then((data)=>{
            if(data == null){
                let err = new Error("data akun tidak ada di master jurnal aplikasi")
                throw err
            }
            return data
        }).catch((err)=>{throw err})

        let newKodeTransaksi = await trxJurnal.max('kode_transaksi',{where:{tahun_rkatu:req.body.tahun}}).then((kodMax)=>{
            if(kodMax == null){
                kodMax = req.body.tahun*angkaKali
            }
                return kodMax+1
        }).catch((err)=>{throw err})

        let check = await trxJurnal.findOne({where:{
            modul:req.body.modul,
            aplikasi:req.body.aplikasi,
            kode_surat:req.body.kode_surat,
            kode_sub_surat:req.body.kode_sub_surat,
            tahun_rkatu:req.body.tahun,
        }}).then((cek)=>{
                if(cek){
                let err = new Error("Data Sudah Ada di dalam Catatan Jurnal")
                throw err
                }
            }).catch((err)=>{throw err})

        let storeJurnal = await trxJurnal.create({
            kode_transaksi:newKodeTransaksi,
            tanggal_transaksi:req.body.tanggal_transaksi,
            keterangan:req.body.keterangan,
            modul:req.body.modul,
            aplikasi:req.body.aplikasi,
            kode_surat:req.body.kode_surat,
            kode_sub_surat:req.body.kode_sub_surat,
            jurnal_aktiva:penyeimbang.jurnal_Debit2.uraian_akun_6,
            jurnal_pasiva:penyeimbang.jurnal_Kredit2.uraian_akun_6,
            akun_aktiva:req.body.akun_bas,
            akun_pasiva:penyeimbang.Kredit,
            Aktiva:req.body.nominal,
            Pasiva:req.body.nominal,
            kode_rkatu:req.body.kode_rkatu,
            bulan_akhir:req.body.bulan_akhir,
            tahun_rkatu:req.body.tahun,
            ucr:req.body.ucr
    }).catch((err)=>{throw err})

    return jsonFormat(res, "success", "berhasil menampilkan Transaksi daata", storeJurnal)
}catch(err){
    err.statusCode = 401
return next(err)
}
}

exports.storeRealExpend = async(req,res,next)=>{
    try{

        let penyeimbang = await refMasterJurnal.findOne({attributes:['Kredit'],where:{kode_aplikasi:req.body.kode_aplikasi,kode_modul:req.body.kode_modul,Debit:req.body.akun_bas}})
        .then(async(data)=>{
            if(data == null){
                let err = new Error("data akun plan tidak ada di master jurnal aplikasi")
                err.statusCode = 201
                throw err}
           return penyeimbang2 = await refMasterJurnal.findOne({include:['jurnal_Debit2','jurnal_Kredit2']
            ,where:{kode_aplikasi:req.body.kode_aplikasi,kode_modul:req.body.kode_modul,Debit:data.Kredit}})
           .then((data2)=>{
            if(data == null){
                let err = new Error("data akun realisasi tidak ada di master jurnal aplikasi")
                err.statusCode = 202
                throw err}
            return data2
           }).catch((err)=>{throw err})
        }).catch((err)=>{throw err})

        let newKodeTransaksi = await trxJurnal.max('kode_transaksi',{where:{tahun_rkatu:req.body.tahun}}).then((kodMax)=>{
            if(kodMax == null){
                kodMax = req.body.tahun*angkaKali
            }
                return kodMax+1
        }).catch((err)=>{throw err})

        let kodeRealisasi = await trxJurnal.findOne({attributes:['kode_transaksi'], where:{
            modul:req.body.modul,
            aplikasi:req.body.aplikasi,
            kode_surat:req.body.kode_surat,
            tahun_rkatu:req.body.tahun,
            reversal:0,
            realisasi:0
        }
    }).then((real)=>{
        if(real == null){
            let err = new Error("data realisasi tidak ada di plan jurnal")
            err.statusCode = 203
            throw err}
        return real
    }).catch((err)=>{throw err})
    console.log(kodeRealisasi)

    let check = await trxJurnal.findOne({where:{
        modul:req.body.modul,
        aplikasi:req.body.aplikasi,
        realisasi:kodeRealisasi.kode_transaksi,
        kode_surat:req.body.kode_surat,
        kode_sub_surat:req.body.kode_sub_surat,
        tahun_rkatu:req.body.tahun,
    }}).then((cek)=>{
            if(cek){
            let err = new Error("Data Sudah Ada di dalam Catatan Jurnal")
            err.statusCode = 204
            throw err
            }
        }).catch((err)=>{throw err})

        let storeJurnal = await trxJurnal.create({
            kode_transaksi:newKodeTransaksi,
            realisasi:kodeRealisasi.kode_transaksi,
            tanggal_transaksi:req.body.tanggal_transaksi,
            keterangan:req.body.keterangan,
            modul:req.body.modul,
            aplikasi:req.body.aplikasi,
            kode_surat:req.body.kode_surat,
            kode_sub_surat:req.body.kode_sub_surat,
            jurnal_aktiva:penyeimbang.jurnal_Debit2.uraian_akun_6,
            jurnal_pasiva:penyeimbang.jurnal_Kredit2.uraian_akun_6,
            akun_aktiva:penyeimbang.Debit,
            akun_pasiva:penyeimbang.Kredit,
            Aktiva:req.body.nominal,
            Pasiva:req.body.nominal,
            norek_aktiva:req.body.norek_aktiva,
            norek_pasiva:req.body.norek_pasiva,
            remark:req.body.remark,
            kode_rkatu:req.body.kode_rkatu,
            bulan_akhir:req.body.bulan_akhir,
            tahun_rkatu:req.body.tahun,
            ucr:req.body.ucr
    }).catch((err)=>{throw err})


    return jsonFormat(res,"success","berhasil membuat Transaksi jurnal", storeJurnal)
    }
    catch(err){
    return next(err)
    }
}

exports.storeReversExpend = async(req,res,next)=>{
    try{
        let penyeimbang = await refMasterJurnal.findOne({include:['jurnal_Debit2','jurnal_Kredit2'],where:{kode_aplikasi:req.body.kode_aplikasi,kode_modul:req.body.kode_modul,Debit:req.body.akun_bas}}).then((data)=>{
            if(data == null){
                let err = new Error("data akun tidak ada di master jurnal aplikasi")
                throw err
            }
            return data
        }).catch((err)=>{throw err})

        let kodeReversal = await trxJurnal.findOne({attributes:['kode_transaksi'], where:{
            modul:req.body.modul,
            aplikasi:req.body.aplikasi,
            kode_surat:req.body.kode_surat,
            tahun_rkatu:req.body.tahun,
            reversal:0,
            realisasi:0
        }
    }).then((real)=>{
        if(real == null){
            let err = new Error("data yang akan di reversal tidak ada di plan jurnal")
            throw err}
        return real
    }).catch((err)=>{throw err})

    let check = await trxJurnal.findOne({where:{
        modul:req.body.modul,
        aplikasi:req.body.aplikasi,
        reversal:kodeReversal.kode_transaksi,
        kode_surat:req.body.kode_surat,
        kode_sub_surat:req.body.kode_sub_surat,
        tahun_rkatu:req.body.tahun,
    }}).then((cek)=>{
            if(cek){
            let err = new Error("Data Sudah Ada di dalam Catatan Jurnal")
            err.statusCode = 204
            throw err
            }
        }).catch((err)=>{throw err})

    let newKodeTransaksi = await trxJurnal.max('kode_transaksi',{where:{tahun_rkatu:req.body.tahun}}).then((kodMax)=>{
        if(kodMax == null){
            kodMax = req.body.tahun*angkaKali
        }
            return kodMax+1
    }).catch((err)=>{throw err})

    let storeJurnal = await trxJurnal.create({
        kode_transaksi:newKodeTransaksi,
        reversal:kodeReversal.kode_transaksi,
        tanggal_transaksi:req.body.tanggal_transaksi,
        keterangan:req.body.keterangan,
        modul:req.body.modul,
        aplikasi:req.body.aplikasi,
        kode_surat:req.body.kode_surat,
        kode_sub_surat:req.body.kode_sub_surat,
        jurnal_aktiva:penyeimbang.jurnal_Debit2.uraian_akun_6,
        jurnal_pasiva:penyeimbang.jurnal_Kredit2.uraian_akun_6,
        akun_aktiva:penyeimbang.Debit,
        akun_pasiva:penyeimbang.Kredit,
        Aktiva:req.body.nominal,
        Pasiva:req.body.nominal,
        kode_rkatu:req.body.kode_rkatu,
        bulan_akhir:req.body.bulan_akhir,
        tahun_rkatu:req.body.tahun,
        ucr:req.body.ucr
}).catch((err)=>{throw err})

    return jsonFormat(res,"success","berhasil membuat Transaksi jurnal", storeJurnal)

    }catch(err){
        return next(err)
    }
}

exports.indexAplikasi = async(req,res,next)=>{
    try{
        let show = await trxJurnal.findAll({where:{aplikasi:req.params.aplikasi}}).then((data)=>{return data}).catch((err)=>{throw err})
        return jsonFormat(res,"success","berhasil membuat Transaksi jurnal", show)
    }catch(err){
        return next(err)
    }
}

exports.rkaakandipakai = async(req,res,next)=>{
    try{
        let plan = await trxJurnal.findAll({attributes:[
            'kode_rkatu','bulan_akhir',[fn('SUM', col('Aktiva')), 'nominal']
        ],
        group:['kode_rkatu','bulan_akhir'],
    where:{tahun_rkatu:req.params.tahun, reversal:0,realisasi:0},
    row:true
    })

    return jsonFormat(res, "success", "berhasil menampilkan daata", plan)

    }
    catch(err){
        return next(err)
    }
}

exports.rkaterealisasi = async(req,res,next)=>{
    try{
        let plan = await trxJurnal.findAll({attributes:[
            'kode_rkatu','bulan_akhir',[fn('SUM', col('Aktiva')), 'nominal']
        ],
        group:['kode_rkatu','bulan_akhir'],
    where:{tahun_rkatu:req.params.tahun, reversal:0,realisasi:{
        [Op.not]:0
    }},
    row:true
    })

    return jsonFormat(res, "success", "berhasil menampilkan daata", plan)

    }
    catch(err){
        return next(err)
    }
}

exports.rkatidakjadidipakai = async(req,res,next)=>{
    try{
        let plan = await trxJurnal.findAll({attributes:[
            'kode_rkatu','bulan_akhir',[fn('SUM', col('Aktiva')), 'nominal']
        ],
        group:['kode_rkatu','bulan_akhir'],
    where:{tahun_rkatu:req.params.tahun, realisasi:0,reversal:{
        [Op.not]:0
    }},
    row:true
    })

    return jsonFormat(res, "success", "berhasil menampilkan daata", plan)

    }
    catch(err){
        return next(err)
    }
}

exports.rkatercatat = async(req,res,next)=>{
    try{
        let plan = await trxJurnal.findAll({attributes:[
            'kode_rkatu','bulan_akhir',
            [literal(`SUM(CASE WHEN reversal = 0 AND realisasi = 0 THEN Aktiva ELSE 0 END)`), 'rkaakandipakai'],
            [literal(`SUM(CASE WHEN reversal = 0 AND realisasi > 0 THEN Aktiva ELSE 0 END)`), 'rkaterealisasi'],
            [literal(`SUM(CASE WHEN reversal > 0 AND realisasi = 0 THEN Aktiva ELSE 0 END)`), 'rkatidakjadidipakai'],
        ],
        group:['kode_rkatu','bulan_akhir'],
    where:{tahun_rkatu:req.params.tahun},
    row:true
    })

    return jsonFormat(res, "success", "berhasil menampilkan daata", plan)

    }
    catch(err){
        return next(err)
    }
}
