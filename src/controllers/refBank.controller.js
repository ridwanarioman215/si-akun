const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const axios = require("axios");
const { validationResult } = require("express-validator");
const refBank = require("../models/refBank.model");
const path = require("path");
const fs = require("fs");

exports.getAll = async(req,res,next)=>{
    // const ab = await axios .get('http://172.16.100.69:4800/bank').catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    refBank.findAll().then((rb)=>{  
        jsonFormat(res, "success", "Berhasil menampilkan data", rb); 
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})    
}

exports.getById = async(req,res,nex)=>{
  refBank.findOne({where:{nomor_rekening:req.params.nomor_rekening}}).then((rb)=>{
    if(rb==null){
      let error = new Error("data tidak ditemukan")
      throw error
    }
    jsonFormat(res, "success", "Berhasil menampilkan data", rb);
  }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.getByKodeUnit = async(req,res,next)=>{
  refBank.findAll({where:{kode_unit:req.params.kode_unit}}).then((rb)=>{   
      jsonFormat(res, "success", "Berhasil menampilkan data", rb); 
      }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})    
}


exports.createBank = async(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty() && !req.file) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }
    
    if (!req.file) {
      jsonFormat(res, "failed", "file yang diupload tidak sesuai format", []);
    }
  
    const filename = path.parse(req.file.filename).base;
        
    if (!errors.isEmpty()) {
      filePaths =  './src/public/pdf/bank/'+filename;
      fs.unlink(filePaths, (err) => {
        console.log("unlink error", err);
      });
      return jsonFormat(res, "failed", "validation failed", errors);
    }

  try{
   const bank = await refBank.create({
      kode_bank:req.body.kode_bank,
      nama_bank:req.body.nama_bank,
      nomor_rekening:req.body.nomor_rekening,
      kode_unit:req.body.kode_unit,
      atas_nama_rekening:req.body.atas_nama_rekening,
      alamat_bank:req.body.alamat_bank,
      penanggung_jawab: req.body.penanggung_jawab,
      kontak:req.body.kontak,
      keterangan:req.body.keterangan,
      dokumen:filename,
      status_rekening:req.body.status_rekening,
      aktif:1,
      ucr:req.body.ucr
      })
    jsonFormat(res, "success", "Upload berhasil", bank);
  }catch(error){
    let filePaths =  './src/public/pdf/bank/'+filename;
      fs.unlink(filePaths, (err) => {
        console.log("unlink error", err);
      });
    jsonFormat(res, "failed", error.message, []);
  } 
  }

exports.updateData = async(req,res,next)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
  }

  try{
    const bank = await refBank.update({
       kode_bank:req.body.kode_bank,
       nama_bank:req.body.nama_bank,
       atas_nama_rekening:req.body.atas_nama_rekening,
       alamat_bank:req.body.alamat_bank,
       kode_unit:req.body.kode_unit,
       penanggung_jawab: req.body.penanggung_jawab,
       kontak:req.body.kontak,
       keterangan:req.body.keterangan,
       status_rekening:req.body.status_rekening,
       aktif:1,
       uch:req.body.uch
       },{where:{nomor_rekening:req.body.nomor_rekening}})
     jsonFormat(res, "success", "Update berhasil", bank);
   }catch(error){
     jsonFormat(res, "failed", error.message, []);
   } 
}