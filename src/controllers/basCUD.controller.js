const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const { validationResult } = require("express-validator");
const { QueryTypes,Op } = require("sequelize");
const akunsub1 = require("../models/akunMak1.model");
const akunsub2 = require("../models/akunMak2.model");
const akunsub3 = require("../models/akunMak3.model");
const akunsub4 = require("../models/akunMak4.model");
const akunsub5 = require("../models/akunMak5.model");
const akunsub6 = require("../models/akunMak.model");
const normalitas = require("../models/normalitas.model");

exports.creatSub1 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub1.findAll().then((as1)=>{
        let arrdatatable = as1.map((a)=>parseInt(a.kode_akun_1))
        let kode_akun_baru = 1
        if(arrdatatable.length>0){
        for(let a = 1;a<=9;a++){
            if(arrdatatable.includes(a)=== false){
                kode_akun_baru = a;
                break
            }
        }}
        akunsub1.create({
            kode_akun_1 : kode_akun_baru,
            uraian_akun_1 : req.body.uraian_akun_1,
            aktif:1
        }).then((create)=>{ jsonFormat(res, "success", "Berhasil membuat data", create);})
        .catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    
}

exports.creatSub2 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    let kode_parent = parseInt(req.body.kode_akun_1)
    akunsub2.findAll({where:{kode_akun_1:req.body.kode_akun_1}}).then((as2)=>{
    let arrdatatable = as2.map((a)=>parseInt(a.kode_akun_2))
    let kode_akun_baru = kode_parent*10+1;
    let minloop = kode_parent*10+1;
    let maksloop = kode_parent*10+9;
    if(arrdatatable.length>0){
    for(let a = minloop;a<=maksloop;a++){
        if(arrdatatable.includes(a)=== false){
            kode_akun_baru = a;
            break
        }
    }
    }
    akunsub2.create({
        kode_akun_1 : req.body.kode_akun_1,
        kode_akun_2:kode_akun_baru,
        uraian_akun_2 : req.body.uraian_akun_2,
        aktif:1
    }).then((create)=>{ jsonFormat(res, "success", "Berhasil membuat data", create);})
    .catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    
}

exports.creatSub3 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    let kode_parent = parseInt(req.body.kode_akun_2)
    akunsub3.findAll({where:{kode_akun_2:req.body.kode_akun_2}}).then((as3)=>{
    let arrdatatable = as3.map((a)=>parseInt(a.kode_akun_3))
    let kode_akun_baru = kode_parent*10+1;
    let minloop = kode_parent*10+1;
    let maksloop = kode_parent*10+9;
    if(arrdatatable.length>0){
    for(let a = minloop;a<=maksloop;a++){
        if(arrdatatable.includes(a)=== false){
            kode_akun_baru = a;
            break
        }
    }
    }
    console.log(arrdatatable)
    akunsub3.create({
        kode_akun_2 : req.body.kode_akun_2,
        kode_akun_3:kode_akun_baru,
        uraian_akun_3 : req.body.uraian_akun_3,
        aktif:1
    }).then((create)=>{ jsonFormat(res, "success", "Berhasil membuat data", create);})
    .catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    
}

exports.creatSub4 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    let kode_parent = parseInt(req.body.kode_akun_3)
    akunsub4.findAll({where:{kode_akun_3:req.body.kode_akun_3}}).then((as4)=>{
    let arrdatatable = as4.map((a)=>parseInt(a.kode_akun_4))
    let kode_akun_baru = kode_parent*10+1;
    let minloop = kode_parent*10+1;
    let maksloop = kode_parent*10+9;
    if(arrdatatable.length>0){
    for(let a = minloop;a<=maksloop;a++){
        if(arrdatatable.includes(a)=== false){
            kode_akun_baru = a;
            break
        }
    }
    }
    akunsub4.create({
        kode_akun_3 : req.body.kode_akun_3,
        kode_akun_4:kode_akun_baru,
        uraian_akun_4 : req.body.uraian_akun_4,
        aktif:1
    }).then((create)=>{ jsonFormat(res, "success", "Berhasil membuat data", create);})
    .catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    
}

exports.creatSub5 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    let kode_parent = parseInt(req.body.kode_akun_4)
    akunsub5.findAll({where:{kode_akun_4:req.body.kode_akun_4}}).then((as5)=>{
    let arrdatatable = as5.map((a)=>parseInt(a.kode_akun_5))
    let kode_akun_baru = kode_parent*10+1;
    let minloop = kode_parent*10+1;
    let maksloop = kode_parent*10+9;
    if(arrdatatable.length>0){
    for(let a = minloop;a<=maksloop;a++){
        if(arrdatatable.includes(a)=== false){
            kode_akun_baru = a;
            break
        }
    }
    }
    akunsub5.create({
        kode_akun_4 : req.body.kode_akun_4,
        kode_akun_5:kode_akun_baru,
        uraian_akun_5 : req.body.uraian_akun_5,
        aktif:1
    }).then((create)=>{ jsonFormat(res, "success", "Berhasil membuat data", create);})
    .catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    
}


exports.creatSub6 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    let kode_parent = parseInt(req.body.kode_akun_5)
    akunsub6.findAll({where:{kode_akun_5:req.body.kode_akun_5}}).then((as6)=>{
    let arrdatatable = as6.map((a)=>parseInt(a.kode_akun_6))
    let kode_akun_baru = kode_parent*10+1;
    let minloop = kode_parent*10+1;
    let maksloop = kode_parent*10+9;
    if(arrdatatable.length>0){
    for(let a = minloop;a<=maksloop;a++){
        if(arrdatatable.includes(a)=== false){
            kode_akun_baru = a;
            break
        }
    }
    }
    akunsub6.create({
        kode_akun_5 : req.body.kode_akun_5,
        kode_akun_6:kode_akun_baru,
        uraian_akun_6 : req.body.uraian_akun_6,
        aktif:1,
        keterangan:req.body.keterangan
    }).then((create)=>{ jsonFormat(res, "success", "Berhasil membuat data", create)})
    .catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    
}

exports.updaturaianSub1 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub1.findAll({where:{kode_akun_1:req.body.kode_akun_1}}).then((as1)=>{
        if(as1.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub1.update({uraian_akun_1 : req.body.uraian_akun_1,normalitas : req.body.normalitas},{where:{kode_akun_1 : req.body.kode_akun_1}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updaturaianSub2 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub2.findAll({where:{kode_akun_2:req.body.kode_akun_2}}).then((as2)=>{
        if(as2.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub2.update({uraian_akun_2 : req.body.uraian_akun_2},{where:{kode_akun_2 : req.body.kode_akun_2}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updaturaianSub3 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub3.findAll({where:{kode_akun_3:req.body.kode_akun_3}}).then((as3)=>{
        if(as3.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub3.update({uraian_akun_3 : req.body.uraian_akun_3},{where:{kode_akun_3 : req.body.kode_akun_3}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updaturaianSub4 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub4.findAll({where:{kode_akun_4:req.body.kode_akun_4}}).then((as4)=>{
        if(as4.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub4.update({uraian_akun_4 : req.body.uraian_akun_4},{where:{kode_akun_4 : req.body.kode_akun_4}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updaturaianSub5 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub5.findAll({where:{kode_akun_5:req.body.kode_akun_5}}).then((as5)=>{
        if(as5.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub5.update({uraian_akun_5 : req.body.uraian_akun_5},{where:{kode_akun_5 : req.body.kode_akun_5}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updaturaianSub6 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub6.findAll({where:{kode_akun_6:req.body.kode_akun_6}}).then((as6)=>{
        if(as6.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub6.update({uraian_akun_6 : req.body.uraian_akun_6,keterangan:req.body.keterangan},{where:{kode_akun_6 : req.body.kode_akun_6}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}


exports.nestedallsub = async(req,res,next)=>{ 
    akunsub1.findAll({
        include:
        [{
            model: akunsub2,
            include: [
                {
                    model: akunsub3,
                    include: [
                        {
                            model: akunsub4,
                            include: [
                                {
                                    model: akunsub5,
                                    include: [
                                        {
                                            model: akunsub6,
                                            
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        },{model:normalitas}]
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}


exports.selectionNormalitas = async(req,res,next)=>{
    normalitas.findAll().then((data)=>{
        if(data.length === 0){
            res.statusCode = "252"
            jsonFormat(res, "success", "data kosong", data)    
        }else{
            jsonFormat(res, "success", "Berhasil menampilkan data", data)
        }
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}






exports.updataktifSub1 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub1.findAll({where:{kode_akun_1:req.body.kode_akun_1}}).then((as1)=>{
        if(as1.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub1.update({aktif : req.body.aktif},{where:{kode_akun_1 : req.body.kode_akun_1}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updataktifSub2 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub2.findAll({where:{kode_akun_2:req.body.kode_akun_2}}).then((as2)=>{
        if(as2.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub2.update({aktif : req.body.aktif},{where:{kode_akun_2 : req.body.kode_akun_2}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updataktifSub3 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub3.findAll({where:{kode_akun_3:req.body.kode_akun_3}}).then((as3)=>{
        if(as3.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub3.update({aktif : req.body.aktif},{where:{kode_akun_3 : req.body.kode_akun_3}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updataktifSub4 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub4.findAll({where:{kode_akun_4:req.body.kode_akun_4}}).then((as4)=>{
        if(as4.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub4.update({aktif : req.body.aktif},{where:{kode_akun_4 : req.body.kode_akun_4}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updataktifSub5 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub5.findAll({where:{kode_akun_5:req.body.kode_akun_5}}).then((as5)=>{
        if(as5.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub5.update({aktif : req.body.aktif},{where:{kode_akun_5 : req.body.kode_akun_5}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.updataktifSub6 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub6.findAll({where:{kode_akun_6:req.body.kode_akun_6}}).then((as6)=>{
        if(as6.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }
        akunsub6.update({aktif : req.body.aktif},{where:{kode_akun_6 : req.body.kode_akun_6}}).then((update)=>{
            jsonFormat(res, "success", "Berhasil merubah data", update)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.deleteSub1 = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return jsonFormat(res, "failed", "validation failed", errors);
    }
    akunsub1.findAll({where:{kode_akun_1:req.body.kode_akun_1}}).then((as1)=>{
        if(as1.length===0){
            return jsonFormat(res, "failed", "kode akun tidak sesuai",[]);
        }else if(as1[0].aktif===1){
            return jsonFormat(res, "failed", "kode akun aktif tidak dapat dihapus",[]);
        }
        akunsub1.destroy({where:{kode_akun_1:req.body.kode_akun_1,aktif:0}}).then((del)=>{
            jsonFormat(res, "success", "Berhasil menghapus data", del)
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}

exports.getAllSub1 = async(req,res,next)=>{ 
    akunsub1.findAll().then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getAllSub2 = async(req,res,next)=>{ 
    akunsub2.findAll().then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getAllSub3 = async(req,res,next)=>{ 
    akunsub3.findAll().then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getAllSub4 = async(req,res,next)=>{ 
    akunsub4.findAll().then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getAllSub5 = async(req,res,next)=>{ 
    akunsub5.findAll().then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getAllSub6 = async(req,res,next)=>{ 
    akunsub6.findAll().then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getSelectionSub6 = async(req,res,next)=>{ 
    akunsub6.findAll({where:{kode_akun_6:{[Op.like]:'5%'},aktif:1}}).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyIdSub1 = async(req,res,next)=>{ 
    akunsub1.findOne({where:{
        kode_akun_1:req.params.kode_akun_1
    }}).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyIdSub2 = async(req,res,next)=>{ 
    akunsub2.findOne({
        where:{kode_akun_2:req.params.kode_akun_2
    }}).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyIdSub3 = async(req,res,next)=>{ 
    akunsub3.findOne({
        where:{ kode_akun_3:req.params.kode_akun_3}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyIdSub4 = async(req,res,next)=>{ 
    akunsub4.findOne({
        where:{kode_akun_4:req.params.kode_akun_4}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyIdSub5 = async(req,res,next)=>{ 
    akunsub5.findOne({
        where:{kode_akun_5:req.params.kode_akun_5}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyIdSub6 = async(req,res,next)=>{ 
    akunsub6.findOne({
        where:{kode_akun_6:req.params.kode_akun_6}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}


exports.nestedAktif = async(req,res,next) =>{
    await akunsub1.findAll({
        // include:{
        //     model:akunsub2,
        //     include:{
        //         model:akunsub3,
        //         include:{
        //             model:akunsub4,
        //             include:{
        //                 model:akunsub5,
        //                 include:[
        //                     {
        //                         model : akunsub6, 
        //                         where : {aktif : 1}
        //                     }
        //                 ]
                            
                        
        //             }
        //         }
        //     }
        // }
        required : true,
        include : [
            {
                model : akunsub2, 
                required : true,
                include : [
                    {
                        model : akunsub3, 
                        required : true, 
                        include : [
                            {
                                model : akunsub4, 
                                required : true, 
                                include : [
                                    {
                                        model : akunsub5, 
                                        required : true, 
                                        include : [
                                            {
                                                model : akunsub6, 
                                                where : {aktif : 1},
                                                required : true
                                                
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }).then((data)=>{
        if(data.length==0){
            let err = new Error('data Kosong')
        }
        jsonFormat(res,"success","Berhasil Menampilkan Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[])
    })
}

//Foreighkey

exports.getbyFKSub2 = async(req,res,next)=>{ 
    akunsub2.findAll({
        where:{kode_akun_1:req.params.kode_akun_1
    }}).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyFKSub3 = async(req,res,next)=>{ 
    akunsub3.findAll({
        where:{ kode_akun_2:req.params.kode_akun_2}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyFKSub4 = async(req,res,next)=>{ 
    akunsub4.findAll({
        where:{kode_akun_3:req.params.kode_akun_3}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyFKSub5 = async(req,res,next)=>{ 
    akunsub5.findAll({
        where:{kode_akun_4:req.params.kode_akun_4}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}

exports.getbyFKSub6 = async(req,res,next)=>{ 
    akunsub6.findAll({
        where:{kode_akun_5:req.params.kode_akun_5}
    }).then((output)=>{
        jsonFormat(res, "success", "Berhasil menampilkan data", output)
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    });
}
