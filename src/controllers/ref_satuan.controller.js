const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const axios = require("axios");
const { validationResult } = require("express-validator");
const satuanKegiatan = require("../models/ref_satuan.model");

exports.getAll = async(req,res,next)=>{
    await satuanKegiatan.findAll().then((data)=>{
            if(data.length == 0){
                let err = new Error('data tidak ada di database')
            }
            jsonFormat(res, "success", "data SBM berhasil Ditampilkan", data);
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
}

exports.create = async(req,res,next) =>{
    await satuanKegiatan.findOne({where:{nama_satuan:req.body.nama_satuan}}).then((data)=>{
        if(data!=null){
            let err = new Error('Data Sudah Ada')
            throw err
        }
        satuanKegiatan.create({nama_satuan:req.body.nama_satuan,uraian:req.body.uraian}).then((create)=>{
            jsonFormat(res, "success", "data SBM berhasil Ditambahkan", create);
        }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
    }).catch((err)=>{jsonFormat(res, "failed", err.message, []);})
}