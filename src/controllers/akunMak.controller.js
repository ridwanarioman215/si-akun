const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const { QueryTypes,Op } = require('sequelize');
// const shape = require('shape-json');

const akunMak = require("../models/akunMak.model");
const satuanKeg = require("../models/satuanKeg.model");
const satVolume = require("../models/satVol.model");

exports.getAllAkunMak = async (req, res, next) => {
    try {
        const akun_mak = await akunMak.findAll({
            attributes: {
                exclude: ["kode_akun_5"]
            },
            include:
            {
                model: satuanKeg,
                attributes: ["kode_akun_6", "id_satuan", "nama_satuan"],
                include: [
                    {
                        model: satVolume,
                        exclude: ["id_satuan", "id_satvol", "sat_vol1", "sat_vol2", "sat_vol3", "sat_vol4"],
                        attributes: ["jumlah_sbm"],
                    },
                ],
            }
        });
        jsonFormat(res, "success", "Berhasil memuat standar akun mak", akun_mak);
    } catch (error) {
        jsonFormat(res, "failed", error.message, []);
    }
};


exports.getAkunMakById = async (req, res, next) => {
    try {
        const akun_mak = await akunMak.findAll({
            attributes: {
                exclude: ["kode_akun_5"],
            },
            include:
            {
                model: satuanKeg,
                attributes: ["kode_akun_6", "id_satuan", "nama_satuan"],
                include: [
                    {
                        model: satVolume,
                        exclude: ["id_satuan", "id_satvol", "sat_vol1", "sat_vol2", "sat_vol3", "sat_vol4"],
                        attributes: ["jumlah_sbm"],
                    },
                ],
            }, where: {
                kode_akun_6: req.params.id
            }
        });

        if (akun_mak.length === 0)
            return jsonFormat(res, "failed", "kode standar akun mak tidak ada", []);

        jsonFormat(res, "success", "Berhasil memuat standar akun mak", akun_mak);
    } catch (error) {
        jsonFormat(res, "failed", error.message, []);
    }
};