const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const axios = require("axios");
const { QueryTypes,Op } = require("sequelize");
const { validationResult } = require("express-validator");
const sbmKegiatan = require("../models/ref_sbm_kegiatan.model");
const sbmakun = require("../models/refsbmakun.model");
const satuanKegiatan = require("../models/ref_satuan.model");
const akunMak = require("../models/akunMak.model");
const bentukKegiatan = require(`../models/refbentukkegiatan.model`);
const { create } = require("lodash");
const { runInNewContext } = require("vm");

exports.getAll = async(req,res,next)=>{
    await sbmKegiatan.findAll({ include:[
        {
            model: satuanKegiatan,
            attributes: ["kode_satuan","nama_satuan"]
        },{
            model: akunMak,
            attributes: ["kode_akun_6","uraian_akun_6"]
        }]}).then((data)=>{
            if(data.length == 0){
                let err = new Error('data tidak ada di database')
            }
            jsonFormat(res, "success", "data SBM berhasil Ditampilkan", data);
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
}

exports.forEbudgeting = async(req,res,next)=>{
    await akunMak.findAll({ attributes: {
        exclude: ["kode_akun_5"]
    },where:{kode_akun_6:{[Op.like]:'5%'},aktif:1},include:{
        model: sbmKegiatan,
        include:{
            model:satuanKegiatan
        }
    }
    }).then((data)=>{
        if(data.length == 0){
            let err = new Error('data tidak ada di database')
        }
        jsonFormat(res, "success", "data SBM berhasil Ditampilkan", data);
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.forEbudgetingnew = async(req,res,next)=>{
    await akunMak.findAll({ attributes: {
        exclude: ["kode_akun_5"]
    },where:{kode_akun_6:{[Op.like]:'5%'},aktif:1},
    include:{
        model: sbmakun,
        include:[{
            model:bentukKegiatan
        },{
            model:satuanKegiatan
        }]
    }
    }).then((data)=>{
        if(data.length == 0){
            let err = new Error('data tidak ada di database')
        }
        jsonFormat(res, "success", "data SBM berhasil Ditampilkan", data);
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}



exports.getById = async(req,res,next)=>{
    await sbmKegiatan.findAll({where:{kode_sbm:req.params.kode_sbm}, include:[
        {
            model: satuanKegiatan,
            attributes: ["nama_satuan"]
        },{
            model: akunMak,
            attributes: ["uraian_akun_6"]
        }]}).then((data)=>{
            if(data.length == 0){
                let err = new Error('data tidak ada di database')
            }
            jsonFormat(res, "success", "data SBM berhasil Ditampilkan", data);
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
}

exports.createData = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty() && !req.file) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }
    await sbmKegiatan.findAll({where:{kode_akun_6:req.body.kode_akun_6,kode_satuan:req.body.kode_satuan}}).then((data)=>{
        if(data.length > 0){
            let err = new Error("data sudah ada di database")
            throw err
        }
        return sbmKegiatan.create({
            kode_akun_6:req.body.kode_akun_6,
            kode_satuan:req.body.kode_satuan,
            jumlah_sbm:req.body.jumlah_sbm
        }).then((create)=>{
            jsonFormat(res, "success", "data SBM berhasil Dibuat", create);
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.updateData = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }

    await sbmKegiatan.findAll({where:{kode_sbm:req.body.kode_sbm}}).then((data)=>{
        if(data.length == 0){
            let err = new Error("data tidak ada di database")
            throw err
        } 
        return sbmKegiatan.update({
            kode_akun_6:req.body.kode_akun_6,
            kode_satuan:req.body.kode_satuan,
            jumlah_sbm:req.body.jumlah_sbm
        },{where:{kode_sbm:req.body.kode_sbm}}).then((update)=>{
            jsonFormat(res, "success", "data SBM berhasil Diupdate", update);
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.hapusData = async(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }

    await sbmKegiatan.findAll({where:{kode_sbm:req.body.kode_sbm}}).then((data)=>{
        if(data.length == 0){
            let err = new Error("data tidak ada di database")
            throw err
        } 
        return sbmKegiatan.destroy({where:{kode_sbm:req.body.kode_sbm}}).then((hapus)=>{
            jsonFormat(res, "success", "data SBM berhasil Dihapus", hapus);
        }).catch((err)=>{
            jsonFormat(res, "failed", err.message, []);
        })
    }).catch((err)=>{
        jsonFormat(res, "failed", err.message, []);
    })
}

exports.getAllSbm = async(req,res,next)=>{
    await sbmakun.findAll({
        include:[{
            model:akunMak
        },{
            model:satuanKegiatan
        },{
            model:bentukKegiatan
    }]
    }).then((data)=>{
        jsonFormat(res,"success","Menampilkan data SBM",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.getAllSbmnested = async(req,res,next)=>{
    await bentukKegiatan.findAll({
        include:{
            model:sbmakun,
            include:[{
                model:akunMak
            },{
                model:satuanKegiatan
            }]
        }
    }).then((data)=>{
        jsonFormat(res,"success","Menampilkan data SBM",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.byidSBM = async(req,res,next) =>{
    await sbmakun.findOne({
        include:[{
            model:akunMak
        },{
            model:satuanKegiatan
        },{
            model:bentukKegiatan
    }]
    ,where:{kode_sbm:req.params.kode_sbm}}).then((data)=>{
        if(data==null){
            let error = new Error("data tidak ditemukan");
            throw error
        }
        jsonFormat(res,"success","Menampilkan data SBM",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.byFKSBM = async(req,res,next) =>{
    await sbmakun.findAll({
        include:[{
            model:akunMak
        },{
            model:satuanKegiatan
        },{
            model:bentukKegiatan
    }]
    ,where:{kode_bentuk_kegiatan:req.params.kode_bentuk_kegiatan}}).then((data)=>{
        if(data==null){
            let error = new Error("data tidak ditemukan");
            err.statusCode = 422
            throw error
        }
        jsonFormat(res,"success","Menampilkan data SBM",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.createSBM = async(req,res,next) =>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }else{
    await sbmakun.create({
        kode_bentuk_kegiatan:req.body.kode_bentuk_kegiatan,
        komponen:req.body.komponen,
        kode_satuan:req.body.kode_satuan,
        satuan_biaya:req.body.satuan_biaya,
        kode_akun_6:req.body.kode_akun_6,
        keterangan:req.body.keterangan,
        ucr:req.body.ucr
    }).then((data)=>{
        jsonFormat(res,"success","Berhasil Menambahkan Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}
}

exports.updateSBM = async(req,res,next) =>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }else{
    await sbmakun.update({
        kode_bentuk_kegiatan:req.body.kode_bentuk_kegiatan,
        komponen:req.body.komponen,
        kode_satuan:req.body.kode_satuan,
        satuan_biaya:req.body.satuan_biaya,
        kode_akun_6:req.body.kode_akun_6,
        keterangan:req.body.keterangan,
        uch:req.body.uch
    },{where:{kode_sbm:req.body.kode_sbm}}).then((data)=>{
        jsonFormat(res,"success","Berhasil Merubah Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}
}

exports.deleteSBM = async(req,res,next) =>{
    await sbmakun.destroy({where:{kode_sbm:req.params.kode_sbm}}).then((data)=>{
        if(data==0){
            let err = new Error("data tidak ditemukan")
            throw err
        }
        jsonFormat(res,"success","Berhasil Menghapus Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.getAllBentuk = async(req,res,next) =>{
    await bentukKegiatan.findAll().then((data)=>{
        jsonFormat(res,"success","Menampilkan data bentuk kegiatan",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.byidBentuk = async(req,res,next) => {
    await bentukKegiatan.findOne({where:{kode_bentuk_kegiatan:req.params.kode_bentuk_kegiatan}}).then((data)=>{
        if(data==null){
            let error = new Error("data tidak ditemukan");
            throw error
        }
        jsonFormat(res,"success","Menampilkan data bentuk kegiatan",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

exports.createBentuk = async(req,res,next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }else{
    await bentukKegiatan.create({
        uraian:req.body.uraian,
        ucr:req.body.ucr
    }).then((data)=>{
        jsonFormat(res,"success","Berhasil Menambahkan Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}
}

exports.updateBentuk = async(req,res,next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {

      jsonFormat(res, "failed", "Tidak ada data yang diinputkan", errors);
    }else{
    await bentukKegiatan.update({
        uraian:req.body.uraian,
        uch:req.body.uch
    },{where:{kode_bentuk_kegiatan:req.body.kode_bentuk_kegiatan}}).then((data)=>{
        jsonFormat(res,"success","Berhasil merubah Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}
}

exports.deleteBentuk = async(req,res,next) =>{
    await bentukKegiatan.destroy({where:{kode_bentuk_kegiatan:req.params.kode_bentuk_kegiatan}}).then((data)=>{
        if(data==0){
            let err = new Error("data tidak ditemukan")
            throw err
        }
        jsonFormat(res,"success","Berhasil Menghapus Data",data)
    }).catch((err)=>{
        jsonFormat(res,"failed",err.message,[]);
    })
}

