const { jsonFormat } = require("../utils/jsonFormat");
const db = require('../config/database');
const { validationResult } = require("express-validator");
const trxJurnal = require("../models/trx_jurnal.model");
const { QueryTypes,Op,fn,col,literal } = require("sequelize");

exports.rkatercatat = async(req,res,next)=>{
    try{
        let dataRKA = await trxJurnal.findAll({attributes:[
            'kode_rkatu','bulan_akhir',
            [literal(`SUM(CASE WHEN reversal = 0 AND realisasi = 0 THEN Aktiva ELSE 0 END)`), 'rkaakandipakai'],
            [literal(`SUM(CASE WHEN reversal = 0 AND realisasi > 0 THEN Aktiva ELSE 0 END)`), 'rkaterealisasi'],
            [literal(`SUM(CASE WHEN reversal > 0 AND realisasi = 0 THEN Aktiva ELSE 0 END)`), 'rkatidakjadidipakai'],
        ],
        group:['kode_rkatu','bulan_akhir'],
    where:{tahun_rkatu:req.params.tahun},
    row:true
    })

    return jsonFormat(res, "success", "berhasil menampilkan data", dataRKA)

    }
    catch(err){
        return next(err)
    }
}

exports.showrkatercatat = async(req,res,next)=>{
    try{
        let rkadetail = await trxJurnal.findOne({attributes:[
            'kode_rkatu','bulan_akhir',
            [literal(`SUM(CASE WHEN reversal = 0 AND realisasi = 0 THEN Aktiva ELSE 0 END)`), 'rkaakandipakai'],
            [literal(`SUM(CASE WHEN reversal = 0 AND realisasi > 0 THEN Aktiva ELSE 0 END)`), 'rkaterealisasi'],
            [literal(`SUM(CASE WHEN reversal > 0 AND realisasi = 0 THEN Aktiva ELSE 0 END)`), 'rkatidakjadidipakai'],
        ],
        where:{tahun_rkatu:req.params.tahun,kode_rkatu:req.params.kode_rkatu,bulan_akhir:req.params.bulan_akhir},
    row:true
    })

    return jsonFormat(res, "success", "berhasil menampilkan data", rkadetail)

    }
    catch(err){
        return next(err)
    }
}